package energysim.task;

import energysim.sim.Configuration;
import math.SunMath;
import math.TimeMath;
import math.VectorENU;
import model.ProcessData;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Random;

public class WindTask extends ATask
{
    private Random rand;

    @Override
    public void onInit(ProcessData data)
    {
        rand = new Random();
    }

    @Override
    public void onUpdate(ProcessData data)
    {
        long currentTime = data.getTimestamp();
        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochSecond(currentTime), ZoneId.systemDefault());

        int day = date.getDayOfYear();
        double dayDistToMidJune = Math.abs(365/2.0 - day);
        double wind = 3.5 + 2.5 * (dayDistToMidJune/(365/2.0));

        double windOffset = (rand.nextDouble() * 6) - 3;
        wind += windOffset;
        wind = Math.max(0, wind);
        data.setAvgWindSpeed(wind);
    }

    @Override
    public void onExit()
    {

    }
}

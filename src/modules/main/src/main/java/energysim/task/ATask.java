package energysim.task;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public abstract class ATask implements ITask
{
    protected static final Logger logger = LogManager.getLogger(ATask.class.getName());
}

package energysim.task;

import energysim.sim.Configuration;
import math.SunMath;
import math.TimeMath;
import math.VectorENU;
import model.ProcessData;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class SunTask extends ATask
{
    @Override
    public void onInit(ProcessData data)
    {

    }

    @Override
    public void onUpdate(ProcessData data)
    {
        long currentTime = data.getTimestamp();
        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochSecond(currentTime), ZoneId.systemDefault());
        double jd = TimeMath.getJulianDay(date.getYear(), date.getMonthValue(), date.getDayOfMonth(), date.getHour(), date.getMinute(), date.getSecond());
        VectorENU sunVector = SunMath.calcSunVector(jd, Configuration.latitude, Configuration.longitude,
                Configuration.elevation, Configuration.pressure, Configuration.ambientTemperature);
        data.setTimestamp(currentTime);
        data.setSunVector(sunVector);
    }

    @Override
    public void onExit()
    {

    }
}

package energysim.task;

import com.github.g3force.instanceables.IInstanceableEnum;
import com.github.g3force.instanceables.InstanceableClass;

public enum ETask implements IInstanceableEnum
{
    INIT_TASK(new InstanceableClass(InitTask.class)),

    SUN_TASK(new InstanceableClass(SunTask.class)),

    WIND_TASK(new InstanceableClass(WindTask.class)),

    NATION_TASK(new InstanceableClass(NationTask.class), SUN_TASK);


    private final boolean initiallyActive;
    private final InstanceableClass impl;


    ETask(final InstanceableClass impl, final ETask... dependencies)
    {
        this(impl, true, dependencies);
    }


    @SuppressWarnings("unused")
    ETask(final InstanceableClass impl, final boolean initiallyActive,
          final ETask... dependencies)
    {
        this.impl = impl;
        this.initiallyActive = initiallyActive;
    }


    /**
     * @return the initiallyActive
     */
    public final boolean isInitiallyActive()
    {
        return initiallyActive;
    }


    @Override
    public InstanceableClass getInstanceableClass()
    {
        return impl;
    }
}

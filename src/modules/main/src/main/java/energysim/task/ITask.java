package energysim.task;

import model.ProcessData;

public interface ITask
{
    void onInit(ProcessData data);

    void onUpdate(ProcessData data);

    void onExit();
}

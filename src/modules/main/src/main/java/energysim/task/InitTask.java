package energysim.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.*;

import java.io.IOException;
import java.net.URL;

public class InitTask extends ATask
{
    private boolean initialized = false;

    @Override
    public void onInit(ProcessData data)
    {

    }

    @Override
    public void onUpdate(ProcessData data)
    {
        if (initialized)
        {
            return;
        }

        ObjectMapper mapper = new ObjectMapper();
        try
        {
            ClassLoader classLoader = getClass().getClassLoader();
            URL cons_url = classLoader.getResource("consumption_2018.json");
            URL prod_url = classLoader.getResource("production_2018.json");
            URL price_url = classLoader.getResource("price_2019.json");
            Consumption[] consumptions = mapper.readValue(cons_url, Consumption[].class);
            Production[] productions = mapper.readValue(prod_url, Production[].class);
            Price[] prices = mapper.readValue(price_url, Price[].class);

            ProductionProvider productionProvider = new ProductionProvider(productions);
            ConsumptionProvider consumptionProvider = new ConsumptionProvider(consumptions);
            data.setConsumptionProvider(consumptionProvider);
            data.setProductionProvider(productionProvider);
        } catch (IOException e)
        {
            logger.error(e);
        }


        initialized = true;
    }

    @Override
    public void onExit()
    {
        initialized = false;
    }
}

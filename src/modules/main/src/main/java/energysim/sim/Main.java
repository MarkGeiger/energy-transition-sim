package energysim.sim;

import cli.Cli;
import cli.Shell;
import com.github.g3force.configurable.ConfigRegistration;
import configuration.EConfigurable;
import gui.GUI;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class Main
{
    private static final Logger logger = LogManager.getLogger(Main.class.getName());

    static
    {
        ConfigRegistration.registerClass(EConfigurable.USER.toString(), Main.class);
    }

    public static void main(String[] args)
    {
        logger.info("Init System");
        // parse command line arguments
        Cli cli = new Cli(args);
        cli.parse();

        Master master = new Master();
        GUI gui = new GUI(master);
        Shell shell = new Shell(master);

        // forward log messages to GUI
        LogAppender.getInstance().addObserver(gui);
        logger.info("Gui ready");
        logger.info("Shell ready, type \"help\" for more information");

        // register observers for process data and commands
        master.addProcessDataObserver(shell);
        master.addProcessDataObserver(gui);

        // load Configuration
        ConfigRegistration.registerClass(EConfigurable.USER.toString(), Configuration.class);
        for (EConfigurable e : EConfigurable.values())
        {
            ConfigRegistration.loadConfig(e.toString());
            ConfigRegistration.applyConfig(e.toString());
            ConfigRegistration.save(e.toString());
        }

        logger.info("Waiting for start of simulation:");
        master.runMainLoop();

        shell.setRunning(false);
        shell.exit();
        gui.exit();

        // delayed exit
        try
        {
            Thread.sleep(1000);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
        System.exit(0);
    }
}

package energysim.sim;

import com.github.g3force.configurable.ConfigRegistration;
import com.github.g3force.configurable.Configurable;
import configuration.EConfigurable;
import energysim.task.ETask;
import energysim.task.ITask;
import interfaces.ICommandObserver;
import interfaces.IProcessDataObserver;
import model.Consumption;
import model.ProcessData;
import model.Production;
import model.supply.TransitionModel;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import recording.CSVExporter;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;

public class Master implements ICommandObserver
{
    private static final Logger logger = LogManager.getLogger(Master.class.getName());

    @Configurable(comment = "Start simulation automatically on startup")
    private static boolean autoStart = false;

    private final Map<ETask, ITask> taskMap = new EnumMap<>(ETask.class);

    static
    {
        ConfigRegistration.registerClass(EConfigurable.USER.toString(), Master.class);
    }

    private volatile boolean running = true;
    private List<IProcessDataObserver> observers = new ArrayList<>();
    private boolean paused = true;

    public Master()
    {
        // default
    }

    private void initSystem()
    {
        running = true;
        observers = new ArrayList<>();
        paused = !autoStart;

        for (ETask task : ETask.values())
        {
            try
            {
                taskMap.put(task, (ITask) task.getInstanceableClass().newDefaultInstance());
            } catch (Exception e)
            {
                logger.error("Could not create instance of Task: " + task.toString());
            }
        }
    }

    private void exitSystem()
    {
        taskMap.forEach((k, v) -> v.onExit());
        taskMap.clear();
        observers.clear();
    }

    public void addProcessDataObserver(IProcessDataObserver observer)
    {
        observers.add(observer);
    }

    public void runMainLoop()
    {
        initSystem();
        long currentTime = Configuration.startTime;

        ProcessData data = new ProcessData();
        data.setTimestamp(currentTime);
        CSVExporter recorder = new CSVExporter("./record_" + LocalDateTime.now().toString(), false);
        recorder.setHeader(Arrays.stream(RecordValues.class.getEnumConstants()).map(Enum::name).toArray(String[]::new));

        TransitionModel transitionModel = new TransitionModel();
        transitionModel.init(data);

        taskMap.forEach((k, v) -> v.onInit(data));
        long steps = 0;
        while (running)
        {
            busyWaitWhenPaused();

            // do actual frame calculations here
            data.setTimestamp(currentTime);

            taskMap.forEach((k, v) -> v.onUpdate(data));

            // notify listeners for update
            observers.forEach(IProcessDataObserver::onProcessDataUpdate);

            long numOfStepsNeeded = (Configuration.endTime - Configuration.startTime) / Configuration.stepsSize;
            if (steps % (numOfStepsNeeded / 100) == 0)
            {
                int perc = (int) (((double) (steps) / numOfStepsNeeded) * 100);
                logger.info(perc + "% ...");
            }

            if (currentTime > Configuration.endTime)
            {
                // finished Calculations
                logger.info("Finished Calculation, preparing shutdown");
                running = false;
            }

            transitionModel.onUpdate(data);

            Production production = data.getProductionProvider().getProduction(data.getTimestamp());
            Consumption consumption = data.getConsumptionProvider().getConsumption(data.getTimestamp());

            double progress = Math.min(1, Math.max(0, (double) (steps) / numOfStepsNeeded));
            double solarTransitionPower = transitionModel.getSolarSupply();
            double windTransitionPower = transitionModel.getWindSupply();

            double biomass = production.getProduction().getOrDefault("biomass", 0.0);
            double geothermal = production.getProduction().getOrDefault("geothermal", 0.0);
            double hydro = production.getProduction().getOrDefault("hydro", 0.0);
            double solar = production.getProduction().getOrDefault("solar", 0.0) * (1 - progress) + solarTransitionPower;
            double wind = production.getProduction().getOrDefault("wind", 0.0) * (1 - progress) + windTransitionPower;
            double unknown = production.getProduction().getOrDefault("unknown", 0.0);
            double nuclear = production.getProduction().getOrDefault("nuclear", 0.0) * (1 - progress);
            double coal = production.getProduction().getOrDefault("coal", 0.0) * (1 - progress);
            double gas = production.getProduction().getOrDefault("gas", 0.0);
            double oil = production.getProduction().getOrDefault("oil", 0.0) * (1 - progress);

            double hydroStorage = production.getStorage().getOrDefault("hydro", 0.0);

            double cons = consumption.getConsumption() * (1 + 2 * progress);


            double totalProduction = biomass + geothermal + hydro + solar + wind + unknown + nuclear + coal + gas + oil;
            double surplus = totalProduction - cons;

            transitionModel.updateStorages(data, surplus);
            double batteryStorageProduction = transitionModel.getProducedBatteryStorageEnergy();
            double batteryStorageConsumption = transitionModel.getConsumedBatteryStorageEnergy();

            totalProduction += batteryStorageProduction;
            cons += batteryStorageConsumption;
            surplus = surplus + batteryStorageProduction - batteryStorageConsumption;

            recorder.addValues(currentTime, data.getSunVector().getU()*10.0,
                    cons,
                    biomass,
                    geothermal,
                    hydro,
                    solar,
                    wind,
                    unknown,
                    nuclear,
                    coal,
                    gas,
                    oil,
                    hydroStorage,
                    totalProduction,
                    surplus,
                    data.getAvgWindSpeed(),
                    batteryStorageProduction,
                    -batteryStorageConsumption
                    // storage
            );
            currentTime += Configuration.stepsSize;
            steps++;
        }
        logger.info("Exiting Simulation ...");
        String filePath = recorder.close();
        startVisualizer(filePath);

        logger.info("Shutdown ...");
        exitSystem();
    }

    private void startVisualizer(String filePath)
    {
        ClassLoader classLoader = getClass().getClassLoader();
        URL consUrl = classLoader.getResource("plot.py");
        if (consUrl != null)
        {
            try
            {
                ProcessBuilder pb = new ProcessBuilder("python3", consUrl.getPath(), filePath, LocalDateTime.now().toString());
                pb.start();
                logger.info("Result Visualizer started");
            } catch (IOException e)
            {
                logger.error(e);
            }
        } else
        {
            logger.error("Could not find plotting source");
        }
    }

    private void busyWaitWhenPaused()
    {
        while (paused)
        {
            try
            {
                Thread.sleep(500);
            } catch (InterruptedException e)
            {
                logger.log(Level.INFO, "Could not sleep", e);
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public String onPause()
    {
        paused = true;
        return "paused";
    }

    @Override
    public String onResume()
    {
        paused = false;
        return "resumed";
    }

    public enum RecordValues
    {
        TIME_S,
        SUN_Z_NORMALIZED_DIR,
        CONSUMPTION,
        BIOMASS,
        GEOTHERMAL,
        HYRDO,
        SOLAR,
        WIND,
        UNKNWON,
        NUCLEAR,
        COAL,
        GAS,
        OIL,
        STORAGE_HYDRO,
        TOTAL_PRODUCTION,
        SURPLUS,
        WIND_SPEED,
        BATTERY_PRODUCED,
        BATTERY_CONSUMED
        // storage
    }
}

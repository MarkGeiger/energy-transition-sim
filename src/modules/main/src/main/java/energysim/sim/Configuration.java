package energysim.sim;

import com.github.g3force.configurable.ConfigRegistration;
import com.github.g3force.configurable.Configurable;
import configuration.EConfigurable;
import math.TimeMath;

public class Configuration
{
    @Configurable(comment = "Average pressure in Location [mBar]")
    public static double pressure = 1.01325 * 1000;

    @Configurable(comment = "ambient temperature [C]")
    public static double ambientTemperature = 20;

    @Configurable(comment = "Time to start simulation [posix s]")
    public static long startTime = 1514764800L; // 2018.01.01

    @Configurable(comment = "Time to end simulation [posix s]")
    public static long endTime = 2523916800L;

    @Configurable(comment = "Simulate in given step size [s]")
    public static long stepsSize = 60 * 60;

    @Configurable
    public static double longitude = 9.1833333;

    @Configurable
    public static double latitude = 48.7666667;

    @Configurable
    public static double elevation = 247;
}

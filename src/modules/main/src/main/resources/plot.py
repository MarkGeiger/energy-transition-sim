import os
import sys

import matplotlib
import itertools
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
import matplotlib.dates as dm

timestamps = []
values = []
dates = []

f = open(sys.argv[1], 'r')
text = "Energy Transition Simulation"

dont = 0
count = 0
firstrun = 1
timeOffset = 0.0
for line in f:
    dont = dont + 1
    if dont > 5:
        newLine = line.replace('\n', ' ').replace('\r', '')
        mylist = newLine.split(',')
        i = 0
        for value in mylist:
            if firstrun == 1:
                values.append([])
            if i == 0:
                if firstrun:
                    timeOffset = float(value)
                timestamps.append(float(value) - timeOffset)
                time = dt.datetime.fromtimestamp(int(value))
                dates.append(time)
            elif i == 1:
                values[i - 1].append(max(0.0, float(value)))
            else:
                values[i - 1].append(float(value))
            i = i + 1
        firstrun = 0

# SUN_Z_NORMALIZED_DIR,
# CONSUMPTION,
# BIOMASS,
# GEOTHERMAL,
# HYRDO,
# SOLAR,
# WIND,
# UNKNWON,
# NUCLEAR
# COAL,
# GAS,
# OIL

xticks = []
items = range(0, 50)
for i in items[1::2]:
    xticks.append(60 * 60 * 24 * 365 * i)

my_dpi = 80
plt.figure(figsize=(2400 / my_dpi, 2500 / my_dpi), dpi=my_dpi)

# plt.suptitle(text, fontsize=35, fontweight='bold')

font = {'family': 'DejaVu Sans',
        'weight': 'bold',
        'size': 12}
matplotlib.rc('font', **font)
plt.rc('legend', fontsize=10)    # legend fontsize

ax = plt.subplot(411)
plt.plot(dates, values[0], color='#fcd949', linewidth=2.0, aa=True, label=r"Sun_POS_U_Normalized * 10", linestyle='-')
plt.plot(dates, values[15], color='#444444', linewidth=1.0, aa=True, label=r"AVG_WIND_SPEED", linestyle='-')
plt.ylabel('Sun Pos / Wind speed (m/s)')
ax.legend(loc='upper right')

ax.grid(True)
xax = ax.get_xaxis()  # get the x-axis
adf = xax.get_major_formatter()  # the the auto-formatter
adf.scaled[1. / 24] = '%Y-%m-%d %H:%M'  # set the < 1d scale to H:M
adf.scaled[1.0] = '%Y-%m-%d'  # set the > 1d < 1m scale to Y-m-d
adf.scaled[30.] = '%Y-%m'  # set the > 1m < 1Y scale to Y-m
adf.scaled[365.] = '%Y'  # set the > 1y scale to Y
plt.setp(plt.xticks()[1], rotation=30, ha='right')

# plt.tick_params(axis='both', which='major', labelsize=33)
# plt.tick_params(axis='both', which='minor', labelsize=33)

ax = plt.subplot(412)
#       bio        geo         hydro      nuclear    coal       gas        oil          solar     wind     unknown
pal = ["#34d952", "#bd8f40", "#2804d9", "#de1818", "#595959", "#a8e1e3", "#000000", "#ffd500", "#718abd", "#aaaaaa"]
labels = ["Biomass ", "Geothermal", "Hydro", "Nuclear", "Coal", "Gas", "Oil", "Solar", "Wind", "Unknown"]
ax.stackplot(dates, values[2], values[3], values[4], values[8], values[9], values[10], values[11], values[5], values[6],
             values[7], aa=True, colors=pal, labels=labels)
plt.ylabel('Power (MW)')
ax.legend(loc='upper right')
xax = ax.get_xaxis()  # get the x-axis
adf = xax.get_major_formatter()  # the the auto-formatter
adf.scaled[1. / 24] = '%Y-%m-%d %H:%M'  # set the < 1d scale to H:M
adf.scaled[1.0] = '%Y-%m-%d'  # set the > 1d < 1m scale to Y-m-d
adf.scaled[30.] = '%Y-%m'  # set the > 1m < 1Y scale to Y-m
adf.scaled[365.] = '%Y'  # set the > 1y scale to Y
plt.setp(plt.xticks()[1], rotation=30, ha='right')
ax.grid(True)
# plt.tick_params(axis='both', which='major', labelsize=33)
# plt.tick_params(axis='both', which='minor', labelsize=33)

ax = plt.subplot(413)
negHydro = np.array(values[12].copy())
posHydro = np.array(values[12].copy())
posHydro[posHydro < 0] = 0
negHydro[negHydro > 0] = 0
labels = ["Hydro production", "Battery production"]
pal = ["#2233aa", "#ffbb00"]
ax.stackplot(dates, posHydro, values[16], aa=True, colors=pal, labels=labels)
labels = ["Hydro consumption", "Battery consumption"]
pal = ["#223366", "#c46f00"]
ax.stackplot(dates, negHydro, values[17], aa=True, colors=pal, labels=labels)
ax.legend(loc='upper right')
xax = ax.get_xaxis()  # get the x-axis
adf = xax.get_major_formatter()  # the the auto-formatter
adf.scaled[1. / 24] = '%Y-%m-%d %H:%M'  # set the < 1d scale to H:M
adf.scaled[1.0] = '%Y-%m-%d'  # set the > 1d < 1m scale to Y-m-d
adf.scaled[30.] = '%Y-%m'  # set the > 1m < 1Y scale to Y-m
adf.scaled[365.] = '%Y'  # set the > 1y scale to Y
plt.setp(plt.xticks()[1], rotation=30, ha='right')
ax.grid(True)

plt.ylabel('Power (MW)')

ax = plt.subplot(414)
plt.stackplot(dates, values[1], color='#5fd46d', linewidth=1.0, aa=True, labels=["Consumption"])
plt.plot(dates, values[13], color='#fa2020', linewidth=1.0, aa=True, label=r"Total production", linestyle='-')
# values[13] totalProduction
# values[14] surplus
negSurplus = np.array(values[14].copy())
negSurplus[negSurplus > 0] = 0
labels = ["Power shortage"]
pal = ["#f59595"]
ax.stackplot(dates, negSurplus, aa=True, colors=pal, labels=labels)

plt.ylabel('Power (MW)')

ax.grid(True)
xax = ax.get_xaxis()  # get the x-axis
adf = xax.get_major_formatter()  # the the auto-formatter
adf.scaled[1. / 24] = '%Y-%m-%d %H:%M'  # set the < 1d scale to H:M
adf.scaled[1.0] = '%Y-%m-%d'  # set the > 1d < 1m scale to Y-m-d
adf.scaled[30.] = '%Y-%m'  # set the > 1m < 1Y scale to Y-m
adf.scaled[365.] = '%Y'  # set the > 1y scale to Y
plt.setp(plt.xticks()[1], rotation=30, ha='right')

ax.legend(loc='upper right')

plt.subplots_adjust(
    top=0.987,
    bottom=0.046,
    left=0.04,
    right=0.994,
    hspace=0.231,
    wspace=0.2
)
plt.show()

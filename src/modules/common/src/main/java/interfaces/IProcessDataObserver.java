package interfaces;

@FunctionalInterface
public interface IProcessDataObserver
{
    void onProcessDataUpdate();
}

package interfaces;

public interface ICommandObserver
{
    String onPause();

    String onResume();
}

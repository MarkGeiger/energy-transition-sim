package math;

import java.util.Date;
import java.util.Locale;


public class TimeMath
{
    private TimeMath()
    {

    }


    /**
     * Grena function to estimate delta_t= TT(atomic) - UT(earth rotation), s
     *
     * @param jd time as Julian Day
     * @return delta_t, s
     */
    public static double getDeltaT(double jd)
    {
        return 96.4 + 0.00158 * (jd - 2473459.5); // Grena_EPOCH: 2473459.5
    }


    /**
     * uses system time, not JD
     *
     * @return POSIX time stamp in seconds
     */
    public static long getPOSIX()
    {
        Date date = new Date();
        return (long) (date.getTime() * 1e-3);
    }


    /**
     * @return julian day from current time
     */
    public static double getJulianDay()
    {
        Date date = new Date();// POSIX in milliseconds
        return 2440587.500000 + (date.getTime() * 1e-3) / 86400.0; // JD of UNIX epoch + no of days since then
    }


    /**
     * @param year   int year as in civil calender
     * @param month  int month
     * @param day    int day
     * @param hour   int hour (time zone = 0, UT)
     * @param minute int minute
     * @param second int second
     * @return julian day fractional day
     */
    public static double getJulianDay(int year, int month, int day, int hour, int minute, int second)
    {
        double dayDecimal;
        double julianDay;
        int a;

        dayDecimal = day + (hour + (minute + second / 60.0) / 60.0) / 24.0;

        int tMonth = month;
        int tYear = year;
        if (month < 3)
        {
            tMonth += 12;
            tYear--;
        }

        julianDay = Math.floor(365.25 * (tYear + 4716.0)) + Math.floor(30.6001 * (tMonth + 1)) + dayDecimal - 1524.5;

        if (julianDay > 2299160.0)
        {
            // Integer division !
            a = tYear / 100;
            julianDay += (2 - a + a / 4);
        }

        return julianDay;
    }


    /**
     * @param julianDate JD double
     * @return human date "year-month-day hours:minutes:seconds (UTC)"
     */
    public static String getHumanTime(double julianDate)
    {
        int p;
        int q;
        int r;
        int s;
        int t;
        int u;
        int v;
        int year;
        int month;
        int day;
        int hours;
        int minutes;
        double decimalTime;
        double seconds;
        int julianDay;
        //
        double tjulianDate = julianDate + 0.5; // JD counts from noon, humanTime from prior midnight

        decimalTime = (tjulianDate - Math.floor(tjulianDate)) * 24;
        julianDay = (int) tjulianDate;

        hours = (int) Math.floor(decimalTime);
        decimalTime -= hours;
        decimalTime *= 60;
        minutes = (int) Math.floor(decimalTime);
        decimalTime -= minutes;
        seconds = decimalTime * 60;

        p = julianDay + 68569;
        q = 4 * p / 146097;
        r = p - (146097 * q + 3) / 4;
        s = 4000 * (r + 1) / 1461001;
        t = r - 1461 * s / 4 + 31;
        u = 80 * t / 2447;
        v = u / 11;

        year = 100 * (q - 49) + s + v;
        month = u + 2 - 12 * v;
        day = t - 2447 * u / 80;
        return String.format(Locale.US, "%d-%d-%d %d:%d:%.2f (UTC)", year, month, day, hours, minutes, seconds);
    }
}

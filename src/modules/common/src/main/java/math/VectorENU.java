package math;

import java.util.Locale;


public class VectorENU
{
    private double e = 0;
    private double n = 0;
    private double u = 0;


    /**
     *
     */
    public VectorENU()
    {
        // just for JSON parsing
    }


    /**
     * @param e to set
     * @param n to set
     * @param u to set
     */
    public VectorENU(double e, double n, double u)
    {
        this.e = e;
        this.n = n;
        this.u = u;
    }


    /**
     * Copy constructor
     *
     * @param vec create copy of this vector
     */
    public VectorENU(VectorENU vec)
    {
        this.e = vec.getE();
        this.n = vec.getN();
        this.u = vec.getU();
    }


    /**
     * @param x args
     */
    public VectorENU(double... x)
    {
        if (x.length != 3)
        {
            return;
        }
        this.e = x[0];
        this.n = x[1];
        this.u = x[2];
    }

    /**
     * @param value VectorENU as String (json, or other format).
     * @return VectorENU
     */
    public static VectorENU parseString(String value)
    {
        double[] dval = {0, 0, 0};
        String[] values = value.split(",");
        if (values.length != 3)
        {
            throw new IllegalArgumentException("invalid string format");
        }
        for (int i = 0; i < values.length; i++)
        {
            String key = values[i];
            key = key.replaceAll("[(,),{,},\\[,\\]]", "");
            try
            {
                dval[i] = Double.parseDouble(key);
            } catch (NumberFormatException e)
            {
                throw new IllegalArgumentException("invalid string format");
            }
        }
        return new VectorENU(dval[0], dval[1], dval[2]);
    }

    /**
     * @param vector to be added to first vector
     */
    public void add(VectorENU vector)
    {
        this.e += vector.e;
        this.n += vector.n;
        this.u += vector.u;
    }

    /**
     * @param vector to be added to first vector
     * @return new object
     */
    public VectorENU addNew(VectorENU vector)
    {
        double eN = this.e + vector.e;
        double nN = this.n + vector.n;
        double uN = this.u + vector.u;
        return new VectorENU(eN, nN, uN);
    }

    /**
     * @param vector to be added to first vector
     */
    public void subtract(VectorENU vector)
    {
        this.e -= vector.e;
        this.n -= vector.n;
        this.u -= vector.u;
    }

    /**
     * @param vector to be added to first vector
     * @return the Vector
     */
    public VectorENU subtractNew(VectorENU vector)
    {
        double eN = this.e - vector.e;
        double nN = this.n - vector.n;
        double uN = this.u - vector.u;
        return new VectorENU(eN, nN, uN);
    }

    /**
     * @param vector to be multiplied with first vector
     * @return vector dot product.
     */
    public double dot(VectorENU vector)
    {
        return this.e * vector.e + this.n * vector.n + this.u * vector.u;
    }

    /**
     * @return length of ENU vector
     */
    public double length()
    {
        return Math.sqrt(this.e * this.e + this.n * this.n + this.u * this.u);
    }

    /**
     * @param d scalar multiplier
     * @return multiplied vector
     */
    public VectorENU multiplyNew(double d)
    {
        return new VectorENU(this.e * d, this.n * d, this.u * d);
    }

    /**
     * normalizes ENU vector to length 1 m
     */
    public void normalize()
    {
        double norm = Math.sqrt(this.e * this.e + this.n * this.n + this.u * this.u);
        this.e /= norm;
        this.n /= norm;
        this.u /= norm;
    }

    /**
     * normalizes ENU vector to length 1 m
     *
     * @return myself
     */
    public VectorENU normalizeNew()
    {
        double norm = Math.sqrt(this.e * this.e + this.n * this.n + this.u * this.u);
        double eN = this.e / norm;
        double nN = this.n / norm;
        double uN = this.u / norm;
        return new VectorENU(eN, nN, uN);
    }

    /**
     * @return the e
     */
    public double getE()
    {
        return e;
    }

    /**
     * @param e the e to set
     */
    public void setE(double e)
    {
        this.e = e;
    }

    /**
     * @return the n
     */
    public double getN()
    {
        return n;
    }

    /**
     * @param n the n to set
     */
    public void setN(double n)
    {
        this.n = n;
    }

    /**
     * @return the u
     */
    public double getU()
    {
        return u;
    }

    /**
     * @param u the u to set
     */
    public void setU(double u)
    {
        this.u = u;
    }

    @Override
    public String toString()
    {
        return String.format(Locale.US, "(%.2f|%.2f|%.2f)", e, n, u);
    }

    /**
     * @param compareWith vector to compare with this
     * @param tol         tolerance
     * @return true or false
     */
    public boolean equalsTol(VectorENU compareWith, double tol)
    {
        if (Math.abs(compareWith.e - this.e) > tol)
        {
            return false;
        }
        if (Math.abs(compareWith.n - this.n) > tol)
        {
            return false;
        }
        return Math.abs(compareWith.u - this.u) <= tol;
    }


    /**
     * @return e, n, u as double array
     */
    public double[] toArray()
    {
        double[] arr = new double[3];
        arr[0] = this.e;
        arr[1] = this.n;
        arr[2] = this.u;
        return arr;
    }
}

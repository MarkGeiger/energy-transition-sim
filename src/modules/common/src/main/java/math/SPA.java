package math;


public class SPA
{
    private static final int L_COUNT = 6;
    private static final int B_COUNT = 2;
    private static final int R_COUNT = 5;
    private static final int Y_COUNT = 63;


    private static final int termXCOUNT = 5;
    private static final int termYCOUNT = termXCOUNT;

    private static final int[] lSubcount = {64, 34, 20, 7, 3, 1};
    private static final int[] bSubcount = {5, 2};
    private static final int[] rSubcount = {40, 10, 6, 2, 1};
    /*
     * Atmospheric refraction at sunrise and sunset (0.5667 deg is typical), valid range: -5 to 5 degrees,
     * error code: 16
     */
    private static final double atmosRefract = 0.5667;
    /**
     * actual astronomical data
     * Earth Periodic Terms
     */

    private static final double[][][] lTERMS = {
            {
                    {175347046.0, 0, 0},
                    {3341656.0, 4.6692568, 6283.07585},
                    {34894.0, 4.6261, 12566.1517},
                    {3497.0, 2.7441, 5753.3849},
                    {3418.0, 2.8289, 3.5231},
                    {3136.0, 3.6277, 77713.7715},
                    {2676.0, 4.4181, 7860.4194},
                    {2343.0, 6.1352, 3930.2097},
                    {1324.0, 0.7425, 11506.7698},
                    {1273.0, 2.0371, 529.691},
                    {1199.0, 1.1096, 1577.3435},
                    {990, 5.233, 5884.927},
                    {902, 2.045, 26.298},
                    {857, 3.508, 398.149},
                    {780, 1.179, 5223.694},
                    {753, 2.533, 5507.553},
                    {505, 4.583, 18849.228},
                    {492, 4.205, 775.523},
                    {357, 2.92, 0.067},
                    {317, 5.849, 11790.629},
                    {284, 1.899, 796.298},
                    {271, 0.315, 10977.079},
                    {243, 0.345, 5486.778},
                    {206, 4.806, 2544.314},
                    {205, 1.869, 5573.143},
                    {202, 2.458, 6069.777},
                    {156, 0.833, 213.299},
                    {132, 3.411, 2942.463},
                    {126, 1.083, 20.775},
                    {115, 0.645, 0.98},
                    {103, 0.636, 4694.003},
                    {102, 0.976, 15720.839},
                    {102, 4.267, 7.114},
                    {99, 6.21, 2146.17},
                    {98, 0.68, 155.42},
                    {86, 5.98, 161000.69},
                    {85, 1.3, 6275.96},
                    {85, 3.67, 71430.7},
                    {80, 1.81, 17260.15},
                    {79, 3.04, 12036.46},
                    {75, 1.76, 5088.63},
                    {74, 3.5, 3154.69},
                    {74, 4.68, 801.82},
                    {70, 0.83, 9437.76},
                    {62, 3.98, 8827.39},
                    {61, 1.82, 7084.9},
                    {57, 2.78, 6286.6},
                    {56, 4.39, 14143.5},
                    {56, 3.47, 6279.55},
                    {52, 0.19, 12139.55},
                    {52, 1.33, 1748.02},
                    {51, 0.28, 5856.48},
                    {49, 0.49, 1194.45},
                    {41, 5.37, 8429.24},
                    {41, 2.4, 19651.05},
                    {39, 6.17, 10447.39},
                    {37, 6.04, 10213.29},
                    {37, 2.57, 1059.38},
                    {36, 1.71, 2352.87},
                    {36, 1.78, 6812.77},
                    {33, 0.59, 17789.85},
                    {30, 0.44, 83996.85},
                    {30, 2.74, 1349.87},
                    {25, 3.16, 4690.48}
            },
            {
                    {628331966747.0, 0, 0},
                    {206059.0, 2.678235, 6283.07585},
                    {4303.0, 2.6351, 12566.1517},
                    {425.0, 1.59, 3.523},
                    {119.0, 5.796, 26.298},
                    {109.0, 2.966, 1577.344},
                    {93, 2.59, 18849.23},
                    {72, 1.14, 529.69},
                    {68, 1.87, 398.15},
                    {67, 4.41, 5507.55},
                    {59, 2.89, 5223.69},
                    {56, 2.17, 155.42},
                    {45, 0.4, 796.3},
                    {36, 0.47, 775.52},
                    {29, 2.65, 7.11},
                    {21, 5.34, 0.98},
                    {19, 1.85, 5486.78},
                    {19, 4.97, 213.3},
                    {17, 2.99, 6275.96},
                    {16, 0.03, 2544.31},
                    {16, 1.43, 2146.17},
                    {15, 1.21, 10977.08},
                    {12, 2.83, 1748.02},
                    {12, 3.26, 5088.63},
                    {12, 5.27, 1194.45},
                    {12, 2.08, 4694},
                    {11, 0.77, 553.57},
                    {10, 1.3, 6286.6},
                    {10, 4.24, 1349.87},
                    {9, 2.7, 242.73},
                    {9, 5.64, 951.72},
                    {8, 5.3, 2352.87},
                    {6, 2.65, 9437.76},
                    {6, 4.67, 4690.48}
            },
            {
                    {52919.0, 0, 0},
                    {8720.0, 1.0721, 6283.0758},
                    {309.0, 0.867, 12566.152},
                    {27, 0.05, 3.52},
                    {16, 5.19, 26.3},
                    {16, 3.68, 155.42},
                    {10, 0.76, 18849.23},
                    {9, 2.06, 77713.77},
                    {7, 0.83, 775.52},
                    {5, 4.66, 1577.34},
                    {4, 1.03, 7.11},
                    {4, 3.44, 5573.14},
                    {3, 5.14, 796.3},
                    {3, 6.05, 5507.55},
                    {3, 1.19, 242.73},
                    {3, 6.12, 529.69},
                    {3, 0.31, 398.15},
                    {3, 2.28, 553.57},
                    {2, 4.38, 5223.69},
                    {2, 3.75, 0.98}
            },
            {
                    {289.0, 5.844, 6283.076},
                    {35, 0, 0},
                    {17, 5.49, 12566.15},
                    {3, 5.2, 155.42},
                    {1, 4.72, 3.52},
                    {1, 5.3, 18849.23},
                    {1, 5.97, 242.73}
            },
            {
                    {114.0, 3.142, 0},
                    {8, 4.13, 6283.08},
                    {1, 3.84, 12566.15}
            },
            {
                    {1, 3.14, 0}
            }
    };
    private static final double[][][] bTERMS = {
            {
                    {280.0, 3.199, 84334.662},
                    {102.0, 5.422, 5507.553},
                    {80, 3.88, 5223.69},
                    {44, 3.7, 2352.87},
                    {32, 4, 1577.34}
            },
            {
                    {9, 3.9, 5507.55},
                    {6, 1.73, 5223.69}
            }
    };
    private static final double[][][] rTERMS = {
            {
                    {100013989.0, 0, 0},
                    {1670700.0, 3.0984635, 6283.07585},
                    {13956.0, 3.05525, 12566.1517},
                    {3084.0, 5.1985, 77713.7715},
                    {1628.0, 1.1739, 5753.3849},
                    {1576.0, 2.8469, 7860.4194},
                    {925.0, 5.453, 11506.77},
                    {542.0, 4.564, 3930.21},
                    {472.0, 3.661, 5884.927},
                    {346.0, 0.964, 5507.553},
                    {329.0, 5.9, 5223.694},
                    {307.0, 0.299, 5573.143},
                    {243.0, 4.273, 11790.629},
                    {212.0, 5.847, 1577.344},
                    {186.0, 5.022, 10977.079},
                    {175.0, 3.012, 18849.228},
                    {110.0, 5.055, 5486.778},
                    {98, 0.89, 6069.78},
                    {86, 5.69, 15720.84},
                    {86, 1.27, 161000.69},
                    {65, 0.27, 17260.15},
                    {63, 0.92, 529.69},
                    {57, 2.01, 83996.85},
                    {56, 5.24, 71430.7},
                    {49, 3.25, 2544.31},
                    {47, 2.58, 775.52},
                    {45, 5.54, 9437.76},
                    {43, 6.01, 6275.96},
                    {39, 5.36, 4694},
                    {38, 2.39, 8827.39},
                    {37, 0.83, 19651.05},
                    {37, 4.9, 12139.55},
                    {36, 1.67, 12036.46},
                    {35, 1.84, 2942.46},
                    {33, 0.24, 7084.9},
                    {32, 0.18, 5088.63},
                    {32, 1.78, 398.15},
                    {28, 1.21, 6286.6},
                    {28, 1.9, 6279.55},
                    {26, 4.59, 10447.39}
            },
            {
                    {103019.0, 1.10749, 6283.07585},
                    {1721.0, 1.0644, 12566.1517},
                    {702.0, 3.142, 0},
                    {32, 1.02, 18849.23},
                    {31, 2.84, 5507.55},
                    {25, 1.32, 5223.69},
                    {18, 1.42, 1577.34},
                    {10, 5.91, 10977.08},
                    {9, 1.42, 6275.96},
                    {9, 0.27, 5486.78}
            },
            {
                    {4359.0, 5.7846, 6283.0758},
                    {124.0, 5.579, 12566.152},
                    {12, 3.14, 0},
                    {9, 3.63, 77713.77},
                    {6, 1.87, 5573.14},
                    {3, 5.47, 18849.23}
            },
            {
                    {145.0, 4.273, 6283.076},
                    {7, 3.92, 12566.15}
            },
            {
                    {4, 2.56, 6283.08}
            }
    };
    /**
     * Periodic Terms for the nutation in longitude and obliquity
     */

    private static final int[][] yTERMS = {
            {0, 0, 0, 0, 1},
            {-2, 0, 0, 2, 2},
            {0, 0, 0, 2, 2},
            {0, 0, 0, 0, 2},
            {0, 1, 0, 0, 0},
            {0, 0, 1, 0, 0},
            {-2, 1, 0, 2, 2},
            {0, 0, 0, 2, 1},
            {0, 0, 1, 2, 2},
            {-2, -1, 0, 2, 2},
            {-2, 0, 1, 0, 0},
            {-2, 0, 0, 2, 1},
            {0, 0, -1, 2, 2},
            {2, 0, 0, 0, 0},
            {0, 0, 1, 0, 1},
            {2, 0, -1, 2, 2},
            {0, 0, -1, 0, 1},
            {0, 0, 1, 2, 1},
            {-2, 0, 2, 0, 0},
            {0, 0, -2, 2, 1},
            {2, 0, 0, 2, 2},
            {0, 0, 2, 2, 2},
            {0, 0, 2, 0, 0},
            {-2, 0, 1, 2, 2},
            {0, 0, 0, 2, 0},
            {-2, 0, 0, 2, 0},
            {0, 0, -1, 2, 1},
            {0, 2, 0, 0, 0},
            {2, 0, -1, 0, 1},
            {-2, 2, 0, 2, 2},
            {0, 1, 0, 0, 1},
            {-2, 0, 1, 0, 1},
            {0, -1, 0, 0, 1},
            {0, 0, 2, -2, 0},
            {2, 0, -1, 2, 1},
            {2, 0, 1, 2, 2},
            {0, 1, 0, 2, 2},
            {-2, 1, 1, 0, 0},
            {0, -1, 0, 2, 2},
            {2, 0, 0, 2, 1},
            {2, 0, 1, 0, 0},
            {-2, 0, 2, 2, 2},
            {-2, 0, 1, 2, 1},
            {2, 0, -2, 0, 1},
            {2, 0, 0, 0, 1},
            {0, -1, 1, 0, 0},
            {-2, -1, 0, 2, 1},
            {-2, 0, 0, 0, 1},
            {0, 0, 2, 2, 1},
            {-2, 0, 2, 0, 1},
            {-2, 1, 0, 2, 1},
            {0, 0, 1, -2, 0},
            {-1, 0, 1, 0, 0},
            {-2, 1, 0, 0, 0},
            {1, 0, 0, 0, 0},
            {0, 0, 1, 2, 0},
            {0, 0, -2, 2, 2},
            {-1, -1, 1, 0, 0},
            {0, 1, 1, 0, 0},
            {0, -1, 1, 2, 2},
            {2, -1, -1, 2, 2},
            {0, 0, 3, 2, 2},
            {2, -1, 0, 2, 2},
    };
    private static final double[][] peTERMS = {
            {-171996, -174.2, 92025, 8.9},
            {-13187, -1.6, 5736, -3.1},
            {-2274, -0.2, 977, -0.5},
            {2062, 0.2, -895, 0.5},
            {1426, -3.4, 54, -0.1},
            {712, 0.1, -7, 0},
            {-517, 1.2, 224, -0.6},
            {-386, -0.4, 200, 0},
            {-301, 0, 129, -0.1},
            {217, -0.5, -95, 0.3},
            {-158, 0, 0, 0},
            {129, 0.1, -70, 0},
            {123, 0, -53, 0},
            {63, 0, 0, 0},
            {63, 0.1, -33, 0},
            {-59, 0, 26, 0},
            {-58, -0.1, 32, 0},
            {-51, 0, 27, 0},
            {48, 0, 0, 0},
            {46, 0, -24, 0},
            {-38, 0, 16, 0},
            {-31, 0, 13, 0},
            {29, 0, 0, 0},
            {29, 0, -12, 0},
            {26, 0, 0, 0},
            {-22, 0, 0, 0},
            {21, 0, -10, 0},
            {17, -0.1, 0, 0},
            {16, 0, -8, 0},
            {-16, 0.1, 7, 0},
            {-15, 0, 9, 0},
            {-13, 0, 7, 0},
            {-12, 0, 6, 0},
            {11, 0, 0, 0},
            {-10, 0, 5, 0},
            {-8, 0, 3, 0},
            {7, 0, -3, 0},
            {-7, 0, 0, 0},
            {-7, 0, 3, 0},
            {-7, 0, 3, 0},
            {6, 0, 0, 0},
            {6, 0, -3, 0},
            {6, 0, -3, 0},
            {-6, 0, 3, 0},
            {-6, 0, 3, 0},
            {5, 0, 0, 0},
            {-5, 0, 3, 0},
            {-5, 0, 3, 0},
            {-5, 0, 3, 0},
            {4, 0, 0, 0},
            {4, 0, 0, 0},
            {4, 0, 0, 0},
            {-4, 0, 0, 0},
            {-4, 0, 0, 0},
            {-4, 0, 0, 0},
            {3, 0, 0, 0},
            {-3, 0, 0, 0},
            {-3, 0, 0, 0},
            {-3, 0, 0, 0},
            {-3, 0, 0, 0},
            {-3, 0, 0, 0},
            {-3, 0, 0, 0},
            {-3, 0, 0, 0},
    };
    /*
     * apparent sun longitude [degrees]
     */
    double lambda;
    /*
     * geocentric longitude
     */
    double theta;

    // -----------------Intermediate OUTPUT VALUES--------------------
    /*
     * geocentric Latitude
     */
    double beta;
    /*
     * Difference between earth rotation time (TT) and terrestrial time UT(C) (s)
     */
    private double deltat;
    /*
     * Observer longitude (negative west of Greenwich)
     * valid range: -180 to 180 degrees,
     * error code: 9
     */
    private double longitude;
    /*
     * Observer latitude (negative south of equator),
     * valid range: -90 to 90 degrees,
     * error code: 10
     */
    private double latitude;
    /*
     * Observer elevation [meters], valid range: -6500000 or higher meters, error code: 11
     */
    private double elevation;
    /*
     * Annual average local pressure [millibars],
     * valid range: 0 to 5000 millibars, error code: 12
     */
    private double pressure = 1013;
    /*
     * Annual average local temperature [degrees Celsius], valid range: -273 to 6000 degrees Celsius,
     * error code: 13
     */
    private double temperature = 20;
    // Julian day
    private double jd;
    /**
     * earth radius vector [Astronomical Units, AU]
     */
    private double r;
    /*
     * x0 mean elongation (moon-sun) [degrees]
     * x1 mean anomaly (sun) [degrees]
     * x2 mean anomaly (moon) [degrees]
     * x3 argument latitude (moon) [degrees]
     * x4 ascending longitude (moon) [degrees]
     */
    private double[] x = new double[termXCOUNT];
    /*
     * nutation longitude [degrees]
     */
    private double delpsi;
    /*
     * nutation obliquity [degrees]
     */
    private double delepsilon;
    /**
     * Greenwich sidereal time [degrees]
     */
    private double nu;
    /*
     * geocentric sun right ascension [degrees]
     */
    private double alpha;
    /*
     * geocentric sun declination [degrees]
     */
    private double delta;
    /*
     * observer hour angle [degrees]
     */
    private double h;
    /*
     * sun equatorial horizontal parallax [degrees]
     */
    private double xi;
    /*
     * sun right ascension parallax [degrees]
     */
    private double delalpha;
    /*
     * topocentric sun declination [degrees]
     */
    private double deltaprime;
    /*
     * topocentric local hour angle [degrees]
     */
    private double hprime;
    /*
     * topocentric elevation angle (uncorrected) [degrees]
     */
    private double e0;
    /*
     * atmospheric refraction correction [degrees]
     */
    private double dele;
    /*
     * topocentric elevation angle (corrected) [degrees]
     */
    private double e;
    // ---------------------Final OUTPUT VALUES------------------------
    /*
     * topocentric zenith angle [degrees]
     */
    private double zenith;
    /*
     * topocentric azimuth angle (westward from south) [-180 to 180 degrees]
     */
    private double azimuth180;
    /*
     * topocentric azimuth angle (eastward from north) [ 0 to 360 degrees]
     */
    private double azimuth;


    /**
     * constructor
     */
    public SPA()
    {
        // no object
    }


    /**
     * constructor with main initial values
     *
     * @param jd          julian day (days)
     * @param deltat      delta_t = TT-UT (s)
     * @param latitude    coord (degree)
     * @param longitude   coord (degree)
     * @param elevation   elevation (m)
     * @param pAvg        average air pressure, mbars
     * @param tAmbientAvg average ambient temperature (degree C)
     */
    public SPA(double jd, double deltat, double latitude, double longitude, double elevation, double pAvg,
               double tAmbientAvg)
    {
        this.jd = jd;
        this.deltat = deltat;
        this.latitude = latitude;
        this.longitude = longitude;
        this.elevation = elevation;
        this.pressure = pAvg;
        this.temperature = tAmbientAvg;
    }


    /**
     * @return geocentric longitude
     */
    public double getGeocentricLongitude()
    {
        return theta;
    }


    /**
     * @return beta = geocentricLatitude
     */
    public double getGeocentricLatitude()
    {
        return beta;
    }


    /**
     * @return apparent sun longitude [degrees]
     */
    public double getApparentSunLongitude()
    {
        return lambda;
    }


    public double getNutationInLongitude()
    {
        return delpsi;
    }


    /**
     * @return geocentric sun right ascension (degrees)
     */
    public double getGeocentricSunRightAscension()
    {
        return alpha;
    }


    /**
     * @return geocentric sun declination (degrees)
     */
    public double getGeocentricSunDeclination()
    {
        return delta;
    }


    /**
     * @return x double [5]
     * x0 mean elongation (moon-sun) [degrees]
     * x1 mean anomaly (sun) [degrees]
     * x2 mean anomaly (moon) [degrees]
     * x3 argument latitude (moon) [degrees]
     * x4 ascending longitude (moon) [degrees]
     */
    public double[] getX()
    {
        return x;
    }


    /**
     * @return observer hour angle [degrees]
     */
    public double getObserverHourAngle()
    {
        return h;
    }


    /**
     * @return sun equatorial horizontal parallax [degrees]
     */
    public double getSunEquatorialHorizontalParallax()
    {
        return xi;
    }


    /**
     * @return sun right ascension parallax [degrees]
     */
    public double getSunRightAscensionParallax()
    {
        return delalpha;
    }


    /**
     * @return topocentric sun declination [degrees]
     */
    public double getTopocentricSunDeclination()
    {
        return deltaprime;
    }


    /**
     * topocentric local hour angle [degrees]
     */
    public double getTopocentricLocalHourAngle()
    {
        return hprime;
    }


    /**
     * topocentric elevation angle (uncorrected) [degrees]
     */
    public double getTopocentricElevationAngle()
    {
        return e0;
    }


    /**
     * @return atmospheric refraction correction [degrees]
     */
    public double getAtmosphericRefractionCorrection()
    {
        return dele;
    }


    /**
     * @return topocentric elevation angle (corrected) [degrees]
     */
    public double getTopocentricElevationAngleCorrected()
    {
        return e;
    }


    /**
     * @param radians angle in radians
     * @return angle in degrees
     */
    private double rad2deg(double radians)
    {
        return (180.0 / Math.PI) * radians;
    }


    /**
     * @param degrees angle in degrees
     * @return angle in radians
     */
    double deg2rad(double degrees)
    {
        return (Math.PI / 180.0) * degrees;
    }


    double limitDegrees(double degrees)
    {
        double tdegrees = degrees / 360.0;
        double limited = 360.0 * (tdegrees - Math.floor(tdegrees));
        if (limited < 0)
            limited += 360.0;
        return limited;
    }


    double limitDegrees180pm(double degrees)
    {
        double tdegrees = degrees / 360.0;
        double limited = 360.0 * (tdegrees - Math.floor(tdegrees));
        if (limited < -180.0)
            limited += 360.0;
        else if (limited > 180.0)
            limited -= 360.0;
        return limited;
    }


    double limitDegrees180(double degrees)
    {
        double tdegrees = degrees / 180.0;
        double limited = 180.0 * (tdegrees - Math.floor(tdegrees));
        if (limited < 0)
            limited += 180.0;
        return limited;
    }


    double limitZero2one(double value)
    {
        double limited = value - Math.floor(value);
        if (limited < 0)
            limited += 1.0;
        return limited;
    }


    double thirdOrderPolynomial(double a, double b, double c, double d, double x)
    {
        return ((a * x + b) * x + c) * x + d;
    }


    double julianCentury(double jd)
    {
        return (jd - 2451545.0) / 36525.0; // EPOCH 2000/01/01 12:00:00 UT
    }


    double julianEphemerisDay(double jd, double delta_t)
    {
        return jd + delta_t / 86400.0;
    }


    double julianEphemerisCentury(double jde)
    {
        return (jde - 2451545.0) / 36525.0;
    }


    double julianEphemerisMillennium(double jce)
    {
        return jce / 10.0;
    }


    double earthPeriodicTermSummation(double[][] terms, int count, double jme)
    {
        int i;
        double sum = 0;

        for (i = 0; i < count; i++)
            sum += terms[i][0] * Math.cos(terms[i][1] + terms[i][2] * jme);

        return sum;
    }


    double earthValues(double[] term_sum, int count, double jme)
    {
        int i;
        double sum = 0;

        for (i = 0; i < count; i++)
            sum += term_sum[i] * Math.pow(jme, i);

        sum /= 1.0e8;

        return sum;
    }


    double earthHeliocentricLongitude(double jme)
    {
        double[] sum = new double[L_COUNT];
        int i;

        for (i = 0; i < L_COUNT; i++)
        {
            sum[i] = earthPeriodicTermSummation(lTERMS[i], lSubcount[i], jme);
        }

        return limitDegrees(rad2deg(earthValues(sum, L_COUNT, jme)));

    }


    double earthHeliocentricLatitude(double jme)
    {
        double[] sum = new double[B_COUNT];
        int i;

        for (i = 0; i < B_COUNT; i++)
        {
            sum[i] = earthPeriodicTermSummation(bTERMS[i], bSubcount[i], jme);
        }

        return rad2deg(earthValues(sum, B_COUNT, jme));

    }


    double earthRadiusVector(double jme)
    {
        double[] sum = new double[R_COUNT];
        int i;

        for (i = 0; i < R_COUNT; i++)
        {
            sum[i] = earthPeriodicTermSummation(rTERMS[i], rSubcount[i], jme);
        }

        return earthValues(sum, R_COUNT, jme);

    }


    double geocentricLongitude(double l)
    {
        double theta = l + 180.0;

        if (theta >= 360.0)
            theta -= 360.0;
        return theta;
    }


    double geocentricLatitude(double b)
    {
        double beta = -b;
        return beta;
    }


    double meanElongationMoonSun(double jce)
    {
        return thirdOrderPolynomial(1.0 / 189474.0, -0.0019142, 445267.11148, 297.85036, jce);
    }


    double meanAnomalySun(double jce)
    {
        return thirdOrderPolynomial(-1.0 / 300000.0, -0.0001603, 35999.05034, 357.52772, jce);
    }


    double meanAnomalyMoon(double jce)
    {
        return thirdOrderPolynomial(1.0 / 56250.0, 0.0086972, 477198.867398, 134.96298, jce);
    }


    double argumentLatitudeMoon(double jce)
    {
        return thirdOrderPolynomial(1.0 / 327270.0, -0.0036825, 483202.017538, 93.27191, jce);
    }


    double ascendingLongitudeMoon(double jce)
    {
        return thirdOrderPolynomial(1.0 / 450000.0, 0.0020708, -1934.136261, 125.04452, jce);
    }


    double xyTermSummation(int i, double[] x)
    {
        int j;
        double sum = 0;

        for (j = 0; j < termYCOUNT; j++)
            sum += x[j] * yTERMS[i][j];

        return sum;
    }


    void nutationLongitudeAndObliquity(double jce) // double jce, double x[], double del_psi, double del_epsilon)
    {
        int i;
        double xyTermSum;
        double sumPsi = 0;
        double sumEpsilon = 0;

        for (i = 0; i < Y_COUNT; i++)
        {
            xyTermSum = deg2rad(xyTermSummation(i, x));
            sumPsi += (peTERMS[i][0] + jce * peTERMS[i][1]) * Math.sin(xyTermSum);
            sumEpsilon += (peTERMS[i][2] + jce * peTERMS[i][3]) * Math.cos(xyTermSum);
        }

        delpsi = sumPsi / 36000000.0;
        delepsilon = sumEpsilon / 36000000.0;
    }


    double eclipticMeanObliquity(double jme)
    {
        double u = jme / 10.0;
        return 84381.448 + u * (-4680.96 + u * (-1.55 + u * (1999.25 + u * (-51.38 + u * (-249.67 +
                u * (-39.05 + u * (7.12 + u * (27.87 + u * (5.79 + u * 2.45)))))))));
    }


    double eclipticTrueObliquity(double delta_epsilon, double epsilon0)
    {
        return delta_epsilon + epsilon0 / 3600.0;
    }


    double aberrationCorrection(double r)
    {
        return -20.4898 / (3600.0 * r);
    }


    double apparentSunLongitude(double theta, double delpsi, double deltau)
    {
        return theta + delpsi + deltau;
    }


    double greenwichMeanSiderealTime(double jd, double jc)
    {
        return limitDegrees(280.46061837 + 360.98564736629 * (jd - 2451545.0) +
                jc * jc * (0.000387933 - jc / 38710000.0));
    }


    double greenwichSiderealTime(double nu0, double delta_psi, double epsilon)
    {
        return nu0 + delta_psi * Math.cos(deg2rad(epsilon));
    }


    double geocentricSunRightAscension(double lambda, double epsilon, double beta)
    {
        double lambdaRad = deg2rad(lambda);
        double epsilonRad = deg2rad(epsilon);

        return limitDegrees(rad2deg(Math.atan2(Math.sin(lambdaRad) * Math.cos(epsilonRad) -
                Math.tan(deg2rad(beta)) * Math.sin(epsilonRad), Math.cos(lambdaRad))));
    }


    double geocentricSunDeclination(double beta, double epsilon, double lambda)
    {
        double betaRad = deg2rad(beta);
        double epsilonRad = deg2rad(epsilon);

        return rad2deg(Math.asin(Math.sin(betaRad) * Math.cos(epsilonRad) +
                Math.cos(betaRad) * Math.sin(epsilonRad) * Math.sin(deg2rad(lambda))));
    }


    double observerHourAngle()
    {
        return limitDegrees(nu + longitude - alpha);
    }


    double sunEquatorialHorizontalParallax(double r)
    {
        return 8.794 / (3600.0 * r);
    }


    void sunRightAscensionParallaxAndTopocentricDec()
    {
        double latRad = deg2rad(latitude);
        double xiRad = deg2rad(xi);
        double hRad = deg2rad(h);
        double deltaRad = deg2rad(delta);
        double u = Math.atan(0.99664719 * Math.tan(latRad));
        double y = 0.99664717 * Math.sin(u) + elevation * Math.sin(latRad) / 6378140.0;
        double xx = Math.cos(u) + elevation * Math.cos(latRad) / 6378140.0; // local variable != x[]

        delalpha = rad2deg(Math.atan2(-xx * Math.sin(xiRad) * Math.sin(hRad),
                Math.cos(deltaRad) - xx * Math.sin(xiRad) * Math.cos(hRad)));
        deltaprime = rad2deg(Math.atan2((Math.sin(deltaRad) - y * Math.sin(xiRad)) * Math.cos(delalpha),
                Math.cos(deltaRad) - xx * Math.sin(xiRad) * Math.cos(hRad)));
    }


    double topocentricSunRightAscension(double alphaDeg, double deltaAlpha)
    {
        return alphaDeg + deltaAlpha;
    }


    double topocentricLocalHourAngle()
    {
        return h - delalpha;
    }


    double topocentricElevationAngle()
    {
        double latRad = deg2rad(latitude);
        double deltaPrimeRad = deg2rad(deltaprime);

        return rad2deg(Math.asin(Math.sin(latRad) * Math.sin(deltaPrimeRad) +
                Math.cos(latRad) * Math.cos(deltaPrimeRad) * Math.cos(deg2rad(hprime))));
    }


    double atmosphericRefractionCorrection()
    {
        double delE = 0;

        if (e0 >= -1 * atmosRefract)
            delE = (pressure / 1010.0) * (283.0 / (273.0 + temperature)) *
                    1.02 / (60.0 * Math.tan(deg2rad(e0 + 10.3 / (e0 + 5.11))));

        return delE;
    }


    double topocentricElevationAngleCorrected()
    {
        return e0 + dele;
    }


    double topocentricZenithAngle()
    {
        return 90.0 - e;
    }


    double topocentricAzimuthAngleNeg180180()
    {
        double hPrimeRad = deg2rad(hprime);
        double latRad = deg2rad(latitude);

        return rad2deg(Math.atan2(Math.sin(hPrimeRad),
                Math.cos(hPrimeRad) * Math.sin(latRad) - Math.tan(deg2rad(deltaprime)) * Math.cos(latRad)));
    }


    double topocentricAzimuthAngleZero360()
    {
        return azimuth180 + 180.0;
    }


    double sunMeanLongitude(double jme)
    {
        return limitDegrees(280.4664567 + jme * (360007.6982779 + jme * (0.03032028 +
                jme * (1 / 49931.0 + jme * (-1 / 15300.0 + jme * (-1 / 2000000.0))))));
    }


    /**
     * Calculate required SPA parameters to get the right ascension (alpha) and declination (delta)
     */
    void calculateGeocentricSunRightAscensionAndDeclination()
    {
        /*
         * ecliptic mean obliquity [arc seconds]
         */
        double epsilon0;
        /*
         * ecliptic true obliquity [degrees]
         */
        double epsilon;

        /*
         * Julian ephemeris day
         */
        double jde;
        /*
         * Greenwich mean sidereal time [degrees]
         */
        double nu0;
        /*
         * Julian century
         */
        double jc = julianCentury(jd);
        /*
         * Julian ephemeris century
         */
        double jce;
        // Julian ephemeris millennium
        double jme;
        /*
         * earth heliocentric longitude [degrees]
         */
        double l;
        /*
         * earth heliocentric latitude [degrees]
         */
        double b;
        /*
         * aberration correction [degrees]
         */
        double deltau;

        jde = julianEphemerisDay(jd, deltat);
        jce = julianEphemerisCentury(jde);
        jme = julianEphemerisMillennium(jce);

        l = earthHeliocentricLongitude(jme);// uses jme
        b = earthHeliocentricLatitude(jme);// uses jme
        r = earthRadiusVector(jme);// uses jme


        theta = geocentricLongitude(l);// uses l, sets theta
        beta = geocentricLatitude(b);// uses b, sets beta

        x[0] = meanElongationMoonSun(jce);// uses jce
        x[1] = meanAnomalySun(jce);// uses jce
        x[2] = meanAnomalyMoon(jce);// uses jce
        x[3] = argumentLatitudeMoon(jce);// uses jce
        x[4] = ascendingLongitudeMoon(jce);// uses jce

        nutationLongitudeAndObliquity(jce);// jce, x, del_psi, del_epsilon

        epsilon0 = eclipticMeanObliquity(jme);
        epsilon = eclipticTrueObliquity(delepsilon, epsilon0);

        deltau = aberrationCorrection(r);
        lambda = apparentSunLongitude(theta, delpsi, deltau);// uses theta, del_psi, del_tau
        nu0 = greenwichMeanSiderealTime(jd, jc); // uses jd, jc
        nu = greenwichSiderealTime(nu0, delpsi, epsilon);

        alpha = geocentricSunRightAscension(lambda, epsilon, beta);
        delta = geocentricSunDeclination(beta, epsilon, lambda);
    }


    /**
     * main interface to outside
     * calculates all internal variables
     */
    public void calcSunPosition()
    {
        calculateGeocentricSunRightAscensionAndDeclination();

        h = observerHourAngle();
        xi = sunEquatorialHorizontalParallax(r);

        sunRightAscensionParallaxAndTopocentricDec();

        // alpha_prime = topocentric_sun_right_ascension(this.alpha, this.del_alpha); //only for EOT
        hprime = topocentricLocalHourAngle(); // uses h and del_alpha

        e0 = topocentricElevationAngle(); // uses latitude, delta_prime, h_prime
        dele = atmosphericRefractionCorrection(); // uses pressure, temperature, atmos_refract, e0
        e = topocentricElevationAngleCorrected(); // uses e0, del_e

        setZenith(topocentricZenithAngle());// uses e
        azimuth180 = topocentricAzimuthAngleNeg180180();// uses h_prime, latitude,delta_prime

        setAzimuth(topocentricAzimuthAngleZero360());// uses azimuth180
    }


    /**
     * @return the azimuth
     */
    public double getAzimuth()
    {
        return azimuth;
    }


    /**
     * @param azimuth the azimuth to set
     */
    public void setAzimuth(double azimuth)
    {
        this.azimuth = azimuth;
    }


    /**
     * @return the zenith
     */
    public double getZenith()
    {
        return zenith;
    }


    /**
     * @param zenith the zenith to set
     */
    public void setZenith(double zenith)
    {
        this.zenith = zenith;
    }

}


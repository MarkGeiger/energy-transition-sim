package math;


public class SunMath
{

    private SunMath()
    {
        // no object
    }

    /**
     * @param jd            time as Julian Day
     * @param siteLatitude  position (degrees)
     * @param siteLongitude position (degrees)
     * @param siteElevation position (m)
     * @param pAvg          average pressure (mbars)
     * @param tAmbientAvg   average ( (degree C)
     * @return sunVector ENU
     */
    public static VectorENU calcSunVector(double jd, double siteLatitude, double siteLongitude, double siteElevation,
                                          double pAvg, double tAmbientAvg)
    {

        SPA spa = new SPA(jd, TimeMath.getDeltaT(jd), siteLatitude, siteLongitude, siteElevation, pAvg, tAmbientAvg);

        // set jd and delta t here.
        spa.calcSunPosition(); // calculate sun angles
        // calculate Sun vector
        double azi = spa.getAzimuth() * Math.PI / 180.0;
        double zen = spa.getZenith() * Math.PI / 180.0;

        return new VectorENU(Math.sin(azi) * Math.sin(zen), Math.cos(azi) * Math.sin(zen),
                Math.cos(zen));
    }

}

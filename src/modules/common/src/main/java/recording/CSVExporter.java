package recording;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.*;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


/**
 * With {@link CSVExporter} you can export user-defined values to csv-files on disc
 * Usage:
 * create an instance
 * optionally set a header
 * add values as often as applicable
 * close the instance
 * calls to createInstance and addValues can be distributed to different classes.
 */
public final class CSVExporter
{
    private static final Logger logger = LogManager.getLogger(CSVExporter.class.getName());

    private static final String DELIMITER = ",";
    private final String fileName;
    private final Queue<String> values = new LinkedList<>();
    private final Queue<String> header = new LinkedList<>();
    private boolean autoIncrement = false;
    private String additionalInfo = "";
    private File file;
    private BufferedWriter fileWriter;
    private boolean writeHeader = false;
    private int headerSize = 0;

    private boolean isClosed = false;
    private boolean append = false;


    /**
     * @param fileName      path
     * @param autoIncrement increment to save multiple files
     * @param append        append or overwrite
     */
    public CSVExporter(final String fileName, final boolean autoIncrement, final boolean append)
    {
        this.fileName = fileName;
        this.autoIncrement = autoIncrement;
        this.append = append;
    }


    /**
     * @param fileName      sub-dir and name of exported file without .csv ending
     * @param autoIncrement increment to save multiple files
     */
    public CSVExporter(final String fileName, final boolean autoIncrement)
    {
        this(fileName, autoIncrement, false);
    }


    /**
     * @param folder path
     * @param key    key
     * @param stream values stream
     */
    public static void exportList(final String folder, final String key, final List<Number> stream)
    {
        CSVExporter exporter = new CSVExporter(folder + "/" + key, false);
        exporter.addValues(stream);
        exporter.close();
    }


    /**
     * adds a new data set to a file.
     *
     * @param values list of values. note: count of values has to match the header
     */
    public void addValues(final Number... values)
    {
        this.values.clear();
        for (final Number f : values)
        {
            this.values.add(String.valueOf(f));
        }
        persistRecord();
    }


    /**
     * adds a new data set to a file.
     *
     * @param values list of values. note: count of values has to match the header
     */
    public void addValues(final List<Number> values)
    {
        this.values.clear();
        for (final Object f : values)
        {
            this.values.add(String.valueOf(f));
        }
        persistRecord();
    }


    /**
     * This method writes all content the fields of the given object into the file
     *
     * @param bean bean
     * @throws IllegalAccessException If one of the fields of the given bean is not accessible
     */
    public void addValuesBean(final Object bean) throws IllegalAccessException
    {
        values.clear();

        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields)
        {
            values.add(String.valueOf(field.get(bean)));
        }
        persistRecord();
    }


    /**
     * set the csv header (first row of the file)
     * as soon a header is set, it will be written to the file
     *
     * @param header note: value count has to match header count
     */
    public void setHeader(final String... header)
    {
        this.header.clear();
        for (final String string : header)
        {
            this.header.add(string);
        }
        writeHeader = true;
        headerSize = this.header.size();
    }


    /**
     * set the csv header (first row of the file)
     * as soon a header is set, it will be written to the file
     *
     * @param header note: value count has to match header count
     */
    public void setHeader(final Object... header)
    {
        this.header.clear();
        for (final Object string : header)
        {
            this.header.add((String) string);
        }
        writeHeader = true;
        headerSize = this.header.size();
    }


    /**
     * This method uses reflection to get the available field names and fills a header with them
     *
     * @param bean bean
     */
    public void setHeaderBean(final Object bean)
    {
        header.clear();

        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields)
        {
            header.add(field.getName());
        }

        writeHeader = true;
        headerSize = header.size();
    }


    /**
     * The additional info will be printed above the header.
     * Header must be set
     *
     * @param info information
     */
    public void setAdditionalInfo(final String info)
    {
        additionalInfo = info;
    }


    /**
     * do the actual persisting stuff
     * <p>
     * close has to be called manually
     */
    @SuppressWarnings("squid:S2095")
    private void persistRecord()
    {
        try
        {
            if (file == null)
            {
                File dir = new File(fileName).getParentFile();
                if (!dir.exists())
                {
                    boolean created = dir.mkdirs();
                    if (!created)
                    {
                        logger.warn("Could not create export dir: " + dir.getAbsolutePath());
                    }
                }
                int counter = 0;
                if (autoIncrement)
                {
                    while ((file = new File(fileName + counter + ".csv")).exists())
                    {
                        counter++;
                    }
                } else
                {
                    file = new File(fileName + ".csv");
                }

                fileWriter = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(file, append), "UTF-8"));

                if (writeHeader)
                {
                    fileWriter.write("sep=" + DELIMITER + "\n");
                    fileWriter.write("#Energy-transition-sim CSVExporter\n");
                    fileWriter.write("#" + new Date().toString() + "\n");

                    if (!additionalInfo.isEmpty())
                    {
                        fileWriter.write("#" + additionalInfo + "\n");
                    }

                    fileWriter.write(header.poll());
                    for (final String s : header)
                    {
                        fileWriter.write(DELIMITER + s);
                    }
                    fileWriter.write("\n");
                    fileWriter.flush();
                }

            }
            if (writeHeader && (headerSize != values.size()))
            {
                System.out.println("headerSize: " + headerSize + " values.size(): " + values.size());
                throw new CSVExporterException("object count on values must match header", null);
            }
            fileWriter.write(values.poll());
            for (final String s : values)
            {
                fileWriter.write(DELIMITER + s + " ");
            }
            fileWriter.write("\n");
            fileWriter.flush();

        } catch (final FileNotFoundException err)
        {
            throw new CSVExporterException("file not found", err);
        } catch (final IOException err)
        {
            throw new CSVExporterException("io error", err);
        }

    }


    /**
     * @return absoluteFileName
     */
    public String getAbsoluteFileName()
    {
        return new File(fileName + ".csv").getAbsolutePath();
    }


    /**
     * closes the file stream.
     * do not forget to call this method when you are done
     *
     * @return FilePath
     */
    public String close()
    {
        if (fileWriter != null)
        {
            try
            {
                fileWriter.close();
                logger.info("Saved csv file to " + file.getAbsolutePath());
            } catch (final IOException err)
            {
                throw new CSVExporterException("io error while closing the file", err);
            }
        }
        isClosed = true;
        return file.getAbsolutePath();
    }


    /**
     * @return isClosed
     */
    public boolean isClosed()
    {
        return isClosed;
    }


    /**
     * @return isEmpty
     */
    public boolean isEmpty()
    {
        return values.isEmpty();
    }

}

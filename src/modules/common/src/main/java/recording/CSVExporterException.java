package recording;

/**
 * simple wrapper for exceptions that might occur while exporting to csv
 */
public class CSVExporterException extends RuntimeException
{


    /**
     *
     */
    private static final long serialVersionUID = -809995089136885548L;


    /**
     * @param message   exception message
     * @param throwable throwable to throw.
     */
    public CSVExporterException(final String message, final Throwable throwable)
    {
        super(message, throwable);
    }

}

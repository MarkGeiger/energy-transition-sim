package energysim;

import model.AProvider;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AProviderTest extends AProvider
{
    @Test
    public void testStrangeTimeConversion()
    {
        long t1 = this.strangeDatetimeToTimestamp("2018, 1, 1, 0, 0");
        long t2 = this.strangeDatetimeToTimestamp("2018, 1, 2, 0, 0");
        long t3 = this.strangeDatetimeToTimestamp("2018, 1, 3, 0, 0");
        long t4 = this.strangeDatetimeToTimestamp("2018, 1, 10, 0, 0");
        long t5 = this.strangeDatetimeToTimestamp("2018, 1, 22, 0, 0");
        assertEquals(1514764800, t1);
        assertEquals(1514764800 + 86400, t2);
        assertEquals(1514764800 + 86400 * 2, t3);
        assertEquals(1514764800 + 86400 * 9, t4);
        assertEquals(1514764800 + 86400 * 21, t5);
    }
}

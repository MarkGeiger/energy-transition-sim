package model;

import math.VectorENU;


public class ProcessData
{
    private long timestamp = 0;
    private VectorENU sunVector = new VectorENU(0, 0, 0);
    private double avgWindSpeed = 0; // m/s
    private ProductionProvider productionProvider;
    private ConsumptionProvider consumptionProvider;

    public long getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(long timestamp)
    {
        this.timestamp = timestamp;
    }

    public VectorENU getSunVector()
    {
        return sunVector;
    }

    public void setSunVector(VectorENU sunVector)
    {
        this.sunVector = sunVector;
    }

    public ProductionProvider getProductionProvider()
    {
        return productionProvider;
    }

    public void setProductionProvider(ProductionProvider productionProvider)
    {
        this.productionProvider = productionProvider;
    }

    public ConsumptionProvider getConsumptionProvider()
    {
        return consumptionProvider;
    }

    public void setConsumptionProvider(ConsumptionProvider consumptionProvider)
    {
        this.consumptionProvider = consumptionProvider;
    }

    public double getAvgWindSpeed()
    {
        return avgWindSpeed;
    }

    public void setAvgWindSpeed(double avgWindSpeed)
    {
        this.avgWindSpeed = avgWindSpeed;
    }
}

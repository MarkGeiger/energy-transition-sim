package model;

public class Price extends ADataPoint
{
    private String zoneKey;
    private String datetime;
    private String currency;
    private double price;
    private String source;

    public String getZoneKey()
    {
        return zoneKey;
    }

    public void setZoneKey(String zoneKey)
    {
        this.zoneKey = zoneKey;
    }

    public String getDatetime()
    {
        return datetime;
    }

    public void setDatetime(String datetime)
    {
        this.datetime = datetime;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }
}

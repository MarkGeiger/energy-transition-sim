package model.supply;

public enum EEnergySupplyType
{
    SOLAR,
    WIND,
}

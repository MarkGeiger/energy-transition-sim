package model.supply;

import model.ProcessData;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class SolarEnergySupply extends AEnergySupply
{

    // m^2
    private double area = 0;

    public SolarEnergySupply(double area)
    {
        this.area = area;
    }

    @Override
    public void onUpdate(ProcessData data)
    {
        // average GHI germany 1088 kWh / m2 / a
        // cloud coverage considered in average GHI
        double productionPotential = (1000 * 1088 * area) / (365*24); // Watts

        // efficiency assumed slightly higher than average of (15 - 18), since additional multiplication with sunPosZ, which has
        // its peak value at 0.8 for Germany. This causes an overall error, resulting in an reduction of the total output.
        // The average should revolve around 1.0.
        final double efficiency = 0.25;
        setProduction(Math.max(0, productionPotential * efficiency * data.getSunVector().getU()));
    }
}

package model.supply;

import model.ProcessData;

import java.util.Random;

public class WindEnergySupply extends AEnergySupply
{

    // E147Ep5E2 - 5 MW
    private final double rotorDiameter = 147; // m
    private final double rotorArea = Math.pow(rotorDiameter / 2.0, 2) * Math.PI; // m²
    private final double powerPerSquareMeter = 294.6; // W/m²
    private final double powerRating = rotorArea * powerPerSquareMeter; // W
    private final double height = 155; //m
    private final double peakPowerAtWind = 10; // m/s
    private final double minPowerAtWind = 2.5; // m/s
    private final double shutdownSpeed = 25; // m/s

    private double numberOfTurbines = 0;

    private final static Random rand = new Random();

    public WindEnergySupply(double numberOfTurbines)
    {
        this.numberOfTurbines = numberOfTurbines;
    }

    @Override
    public void onUpdate(ProcessData data)
    {
        double windSpeedAtLocation = Math.max(minPowerAtWind, data.getAvgWindSpeed() + (rand.nextDouble() * 4) - 2);
        double windAtHeight = windSpeedAtLocation * Math.pow(height / 10.0, 0.28);
        if (windAtHeight > shutdownSpeed)
        {
            setProduction(0);
            return;
        }

        double totalPower = numberOfTurbines * powerRating *
                Math.max(0, Math.min(peakPowerAtWind - minPowerAtWind, windAtHeight - minPowerAtWind) / (peakPowerAtWind - minPowerAtWind));
        setProduction(totalPower);
    }
}

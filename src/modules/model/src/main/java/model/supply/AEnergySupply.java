package model.supply;

import model.ProcessData;

public abstract class AEnergySupply
{
    // MW
    private double production = 0;

    public double getProduction()
    {
        return production;
    }

    public void setProduction(double production)
    {
        this.production = production;
    }

     public abstract void onUpdate(ProcessData data);
}

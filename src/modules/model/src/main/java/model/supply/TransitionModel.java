package model.supply;

import model.ProcessData;
import model.storage.AStorage;
import model.storage.BatteryStorage;

import java.util.ArrayList;
import java.util.List;

public class TransitionModel
{
    private final List<AEnergySupply> solarSupplies = new ArrayList<>();
    private final List<AEnergySupply> windSupplies = new ArrayList<>();

    private final List<AStorage> batteryStorages = new ArrayList<>();

    private long lastTimestamp;

    private long solarAccumulatedDif = 0;
    private long windAccumulatedDif = 0;

    private long dif;

    public void init(ProcessData data)
    {
        lastTimestamp = data.getTimestamp();
        // Capacity in MWH
        batteryStorages.add(new BatteryStorage(5 * 1e6));
    }

    public void onUpdate(ProcessData data)
    {
        dif = data.getTimestamp() - lastTimestamp;
        solarAccumulatedDif += dif;
        windAccumulatedDif += dif;

        while (solarAccumulatedDif > 60 * 60 * 24 * 100)
        {
            // add 20 million m² of solar power every 100 days
            solarAccumulatedDif -= 60 * 60 * 24 * 100;
            SolarEnergySupply newConstructedSolarPower = new SolarEnergySupply(28_000_000);
            solarSupplies.add(newConstructedSolarPower);
        }

        while (windAccumulatedDif > 60 * 60 * 24 * 100)
        {
            windAccumulatedDif -= 60 * 60 * 24 * 100;
            WindEnergySupply newConstructedWindPower = new WindEnergySupply(280);
            windSupplies.add(newConstructedWindPower);
        }

        solarSupplies.forEach(e -> e.onUpdate(data));
        windSupplies.forEach(e -> e.onUpdate(data));
        lastTimestamp = data.getTimestamp();
    }

    public void updateStorages(ProcessData data, double surplus)
    {
        for (AStorage storage : batteryStorages)
        {
            surplus = storage.onUpdate(surplus, dif);
        }
    }

    public double getProducedBatteryStorageEnergy()
    {
        return batteryStorages.stream().mapToDouble(AStorage::getEnergyProduced).sum();
    }

    public double getConsumedBatteryStorageEnergy()
    {
        return batteryStorages.stream().mapToDouble(AStorage::getEnergyConsumed).sum();
    }

    public double getSolarSupply()
    {
        return solarSupplies.stream().mapToDouble(AEnergySupply::getProduction).sum() / 1e6;
    }

    public double getWindSupply()
    {
        return windSupplies.stream().mapToDouble(AEnergySupply::getProduction).sum() / 1e6;
    }
}

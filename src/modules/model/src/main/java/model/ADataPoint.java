package model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ADataPoint implements Comparable<ADataPoint>
{
    @JsonIgnore
    protected long timestamp;

    public ADataPoint()
    {
        // default for json parsing
    }

    @JsonIgnore
    private ADataPoint(long timestamp)
    {
        this.timestamp = timestamp;
    }

    @JsonIgnore
    public long getTimestamp()
    {
        return timestamp;
    }

    @JsonIgnore
    public void setTimestamp(long timestamp)
    {
        this.timestamp = timestamp;
    }

    @Override
    public int compareTo(ADataPoint o)
    {
        return (int) (this.timestamp - o.getTimestamp());
    }

    @Override
    public boolean equals(Object o)
    {
        if (o instanceof ADataPoint)
        {
            return ((ADataPoint) o).getTimestamp() == this.getTimestamp();
        }
        return false;
    }


    @JsonIgnore
    public static ADataPoint withTimestamp(long timestamp)
    {
        return new ADataPoint(timestamp);
    }
}

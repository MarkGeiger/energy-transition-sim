package model.storage;

public class BatteryStorage extends AStorage
{

    public BatteryStorage(double capacity)
    {
        setChargingEfficiency(0.7);
        setDischargeEfficiency(0.7);
        setCapacity(capacity);
    }

    @Override
    public double onUpdate(double surplus, long timedif)
    {
        if (timedif == 0)
        {
            return surplus;
        }

        double tDifH = timedif / (60.0 * 60.0);
        if (surplus > 0)
        {
            // charge battery
            double leftCapMWH = (capacity - currentCharge);
            double potChargeMWH = surplus * tDifH * getChargingEfficiency();

            // MWH
            double charging = Math.min(leftCapMWH, potChargeMWH);

            assert currentCharge + charging <= capacity;
            setCurrentCharge(currentCharge + charging);

            double consumedEnergy = (charging / tDifH) / getChargingEfficiency();
            setEnergyConsumed(consumedEnergy);
            setEnergyProduced(0);

            assert surplus - consumedEnergy >= 0;
            return surplus - consumedEnergy;
        }

        // discharge battery
        double leftCap = currentCharge;
        double potDischargeMWH = -surplus * tDifH / getDischargeEfficiency();

        double discharge = Math.min(leftCap, potDischargeMWH);

        assert currentCharge - discharge >= 0;
        setCurrentCharge(currentCharge - discharge);

        double producedEnergy = (discharge / tDifH) * getDischargeEfficiency();
        setEnergyProduced(producedEnergy);
        setEnergyConsumed(0);

        assert surplus + producedEnergy <= 0;
        return surplus + producedEnergy;
    }
}

package model.storage;

public abstract class AStorage
{
    protected double capacity; // MWH
    protected double currentCharge;
    protected double chargingEfficiency;
    protected double dischargeEfficiency;
    protected double energyProduced;
    protected double energyConsumed;

    public abstract double onUpdate(double surplus, long timedif);

    public double getCapacity()
    {
        return capacity;
    }

    public void setCapacity(double capacity)
    {
        this.capacity = capacity;
    }

    public double getCurrentCharge()
    {
        return currentCharge;
    }

    public void setCurrentCharge(double currentCharge)
    {
        this.currentCharge = currentCharge;
    }

    public double getChargingEfficiency()
    {
        return chargingEfficiency;
    }

    public void setChargingEfficiency(double chargingEfficiency)
    {
        this.chargingEfficiency = chargingEfficiency;
    }

    public double getDischargeEfficiency()
    {
        return dischargeEfficiency;
    }

    public void setDischargeEfficiency(double dischargeEfficiency)
    {
        this.dischargeEfficiency = dischargeEfficiency;
    }

    public double getEnergyProduced()
    {
        return energyProduced;
    }

    public void setEnergyProduced(double energyProduced)
    {
        this.energyProduced = energyProduced;
    }

    public double getEnergyConsumed()
    {
        return energyConsumed;
    }

    public void setEnergyConsumed(double energyConsumed)
    {
        this.energyConsumed = energyConsumed;
    }
}

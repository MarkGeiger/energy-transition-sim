package model;

import java.lang.reflect.Array;
import java.util.*;

public class ProductionProvider extends AProvider
{
    private final List<Production> productionList;

    public ProductionProvider(Production[] productions)
    {
        // fill production Map
        productionList = Arrays.asList(productions);
        productionList.forEach(
                e -> e.setTimestamp(strangeDatetimeToTimestamp(e.getDatetime()))
        );

        long stepSize = productionList.get(1).getTimestamp() - productionList.get(0).getTimestamp();
        for (int i = 1; i < productionList.size(); i++)
        {
            long tempStep = productionList.get(i).getTimestamp() - productionList.get(i-1).getTimestamp();
            if (tempStep != stepSize)
            {
                logger.error("Step size changed :/ data corrupted?: " + tempStep + " pos:" + i +  " date: " + productionList.get(i).getDatetime());
            }
        }
        logger.info("parsed production values: " + productionList.size());
        logger.info("production step size: " + stepSize);
    }

    public Production getProduction(long timestamp)
    {
        long startTime = productionList.get(0).getTimestamp();
        long endTime = productionList.get(productionList.size()-1).getTimestamp();

        long tempTimestamp = timestamp;
        while (tempTimestamp > endTime)
        {
            tempTimestamp -= endTime - startTime;
        }

        if (tempTimestamp < startTime || tempTimestamp > endTime)
        {
            logger.error("Production out of range: " + timestamp + " range: " +startTime + "|" + endTime);
        }

        int index = Collections.binarySearch(productionList, ADataPoint.withTimestamp(tempTimestamp));

        if (index < 0)
        {
            logger.error("No Production found " + tempTimestamp + " range: " +startTime + "|" + endTime);
            return productionList.get(0);
        }

        // @TODO: here we should add a linear interpolation.
        // NOTE: It could technically also work to use the stepSize of the data set and the stepSize
        // of the simulation to find the correct index mathematically. But needs a lot of protection.
        // so far performance does not seem to be an issue, so for now use the less performant but stable
        // solution.

        return productionList.get(index);
    }
}

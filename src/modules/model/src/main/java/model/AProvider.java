package model;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class AProvider
{
    protected static final Logger logger = LogManager.getLogger(AProvider.class.getName());

    protected long strangeDatetimeToTimestamp(String datetime)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy, M, d, H, m");
        LocalDateTime date = LocalDateTime.parse(datetime, formatter);
        Instant instant = date.atZone(ZoneOffset.UTC).toInstant();
        return instant.toEpochMilli()/1000;
    }
}

package model;

public class Consumption extends ADataPoint
{
    private String zoneKey;
    private String datetime;
    private double consumption;
    private String source;

    public String getZoneKey()
    {
        return zoneKey;
    }

    public void setZoneKey(String zoneKey)
    {
        this.zoneKey = zoneKey;
    }

    public String getDatetime()
    {
        return datetime;
    }

    public void setDatetime(String datetime)
    {
        this.datetime = datetime;
    }

    public double getConsumption()
    {
        return consumption;
    }

    public void setConsumption(double consumption)
    {
        this.consumption = consumption;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }
}

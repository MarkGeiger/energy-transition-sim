package model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Map;

public class Production extends ADataPoint
{
    private String zoneKey;
    private String datetime;
    private String currency;

    // "biomass": 5401.0,
    // "geothermal": 3.0,
    // "hydro": 1680.0,
    // "solar": 0.0,
    // "wind": 32538.0,
    // "unknown": 473.0
    // "nuclear": 4942.0,
    // "coal": 8034.0,
    // "gas": 1765.0,
    // "oil": 188.0,
    private Map<String,Double> production;

    // "hydro": -340.0
    private Map<String,Double> storage;
    private String source;

    public String getZoneKey()
    {
        return zoneKey;
    }

    public void setZoneKey(String zoneKey)
    {
        this.zoneKey = zoneKey;
    }

    public String getDatetime()
    {
        return datetime;
    }

    public void setDatetime(String datetime)
    {
        this.datetime = datetime;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public Map<String, Double> getProduction()
    {
        return production;
    }

    public void setProduction(Map<String, Double> production)
    {
        this.production = production;
    }

    public Map<String, Double> getStorage()
    {
        return storage;
    }

    public void setStorage(Map<String, Double> storage)
    {
        this.storage = storage;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    private boolean isRenewable(String key)
    {
        String lowKey = key.toLowerCase();
        return !lowKey.equals("oil")
                && !lowKey.equals("gas")
                && !lowKey.equals("coal")
                && !lowKey.equals("nuclear");
    }

    @JsonIgnore
    public double getProductionSum()
    {
        return production.values().stream().mapToDouble(v -> v).sum();
    }

    @JsonIgnore
    public double getProductionSumEEOnly()
    {
        return production.entrySet().stream().filter( e -> isRenewable(e.getKey()))
                .mapToDouble(Map.Entry::getValue).sum();
    }

    @JsonIgnore
    public double getStorageSum()
    {
        return storage.values().stream().mapToDouble(v -> v).sum();
    }

    @JsonIgnore
    public double getEffectiveOutput()
    {
        return getProductionSum() + getStorageSum();
    }
}

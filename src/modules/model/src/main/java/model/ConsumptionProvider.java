package model;

import java.util.*;

public class ConsumptionProvider extends AProvider
{
    private final List<Consumption> consumptionList;

    public ConsumptionProvider(Consumption[] consumptions)
    {
        // fill production Map
        consumptionList = Arrays.asList(consumptions);
        consumptionList.forEach(
                e -> e.setTimestamp(strangeDatetimeToTimestamp(e.getDatetime()))
        );

        long stepSize = consumptionList.get(1).getTimestamp() - consumptionList.get(0).getTimestamp();
        for (int i = 1; i < consumptionList.size(); i++)
        {
            long tempStep = consumptionList.get(i).getTimestamp() - consumptionList.get(i-1).getTimestamp();
            if (tempStep != stepSize)
            {
                logger.error("Step size changed :/ data corrupted?: " + tempStep + " pos:" + i +  " date: " + consumptionList.get(i).getDatetime());
            }
        }
        logger.info("parsed consumption values: " + consumptionList.size());
        logger.info("consumption step size: " + stepSize);
    }

    public Consumption getConsumption(long timestamp)
    {
        long startTime = consumptionList.get(0).getTimestamp();
        long endTime = consumptionList.get(consumptionList.size()-1).getTimestamp();

        long tempTimestamp = timestamp;
        while (tempTimestamp > endTime)
        {
            tempTimestamp -= (endTime - startTime);
        }

        if (tempTimestamp < startTime || tempTimestamp > endTime)
        {
            logger.error("Consumption out of range: " + timestamp + " range: " +startTime + "|" + endTime);
        }

        int index = Collections.binarySearch(consumptionList, ADataPoint.withTimestamp(tempTimestamp));

        long finalTempTimestamp = tempTimestamp;

        // No Consumption found: 1524756600 range: 1514764800 | 1546299900
        if (index < 0)
        {
            logger.warn("No Consumption found with bin search: " + tempTimestamp + " range: " + startTime + "|" + endTime);
            Optional<Consumption> cons = consumptionList.stream().max((e1, e2) -> (int) (e2.getTimestamp() - finalTempTimestamp));
            if (cons.isPresent())
            {
                logger.warn(tempTimestamp + " " + cons.get().getTimestamp());
                return cons.get();
            }
            logger.error("Could not find consumption.");
            return consumptionList.get(0);
        }

        // @TODO: here we should add a linear interpolation.
        // NOTE: It could technically also work to use the stepSize of the data set and the stepSize
        // of the simulation to find the correct index mathematically. But needs a lot of protection.
        // so far performance does not seem to be an issue, so for now use the less performant but stable
        // solution.

        return consumptionList.get(index);
    }
}

package gui.log;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;


public class LogPanel extends JPanel
{
    private static final transient StyleContext SC = new StyleContext();
    private final transient Style warningStyle = SC.addStyle("warning", null);
    private final transient Style infoStyle = SC.addStyle("info", null);
    private final transient Style errorStyle = SC.addStyle("error", null);

    private final DefaultStyledDocument doc = new DefaultStyledDocument(SC);
    private final JTextPane textEditor = new JTextPane(doc);


    public LogPanel()
    {
        JScrollPane sp = new JScrollPane(this.textEditor);
        warningStyle.addAttribute(StyleConstants.Foreground, Color.orange);
        warningStyle.addAttribute(StyleConstants.FontSize, 16);
        warningStyle.addAttribute(StyleConstants.FontFamily, "serif");
        warningStyle.addAttribute(StyleConstants.Bold, Boolean.TRUE);

        errorStyle.addAttribute(StyleConstants.Foreground, Color.red);
        errorStyle.addAttribute(StyleConstants.FontSize, 16);
        errorStyle.addAttribute(StyleConstants.FontFamily, "serif");
        errorStyle.addAttribute(StyleConstants.Bold, Boolean.TRUE);

        infoStyle.addAttribute(StyleConstants.Foreground, Color.WHITE);
        infoStyle.addAttribute(StyleConstants.FontSize, 12);
        infoStyle.addAttribute(StyleConstants.FontFamily, "serif");
        infoStyle.addAttribute(StyleConstants.Bold, Boolean.TRUE);

        textEditor.setEditable(false);
        textEditor.setBackground(new Color(40, 40, 40));
        textEditor.setForeground(new Color(255, 255, 255));

        setLayout(new BorderLayout());
        add(sp, BorderLayout.CENTER);
    }


    public void onNewLogRecord(LogEvent event)
    {
        Style style;

        if (textEditor.getText().length() > 20000)
        {
            textEditor.setText("Clearing LogPanel - log can also be seen in local file");
        }
        try
        {
            if (event.getLevel() == Level.WARN)
            {
                style = warningStyle;
            } else if (event.getLevel() == Level.ERROR)
            {
                style = errorStyle;
            } else
            {
                style = infoStyle;
            }

            doc.insertString(doc.getLength(), event.getInstant().getEpochSecond() + ": -  " + event.getMessage() + "\n", style);
            textEditor.setCaretPosition(textEditor.getDocument().getLength());
        } catch (BadLocationException e1)
        {
            e1.printStackTrace();
        }
    }
}

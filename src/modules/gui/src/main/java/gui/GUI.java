package gui;

import cli.CommandParser;
import com.github.g3force.configurable.ConfigRegistration;
import configuration.EConfigurable;
import gui.config.EditorView;
import gui.log.LogPanel;
import interfaces.ICommandObserver;
import interfaces.IProcessDataObserver;
import log.ILogObserver;
import org.apache.logging.log4j.core.LogEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URISyntaxException;
import java.net.URL;

public class GUI extends JFrame implements IProcessDataObserver, ActionListener, ILogObserver
{
    private final JTextArea area = new JTextArea();
    private final LogPanel logArea = new LogPanel();
    private final JTextField input = new JTextField();
    private CommandParser parser;
    private EditorView editorView;

    public GUI(ICommandObserver commandObserver)
    {
        parser = new CommandParser(commandObserver);

        setSize(1200, 500);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("logo_s.png");
        assert resource != null;
        ImageIcon frameIcon = new ImageIcon(resource);

        this.setIconImage(frameIcon.getImage());

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        setTitle("Energy Transition Simulator");
        setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane(area);
        area.setEditable(false);
        area.setBackground(Color.BLACK);
        area.setForeground(new Color(30, 170, 0, 255));
        area.setBorder(BorderFactory.createBevelBorder(0));

        logArea.setBorder(BorderFactory.createTitledBorder("Logging"));

        JPanel inputWrapper = new JPanel(new BorderLayout());
        input.addActionListener(this);
        inputWrapper.setBorder(BorderFactory.createTitledBorder("Command interface"));
        inputWrapper.add(input, BorderLayout.CENTER);

        try
        {
            editorView = new EditorView("Configuration", EConfigurable.USER.toString(), ConfigRegistration.getConfig(EConfigurable.USER.toString()), true);
            //add(BorderLayout.EAST, editorView);
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                scrollPane, editorView);

        JSplitPane splitPane2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                logArea, splitPane);

        splitPane2.setOneTouchExpandable(true);
        splitPane2.setDividerLocation(200);

        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(600);
        Dimension minimumSize = new Dimension(300, 50);
        scrollPane.setMinimumSize(minimumSize);
        scrollPane.setBorder(BorderFactory.createTitledBorder("Console output"));
        editorView.setMinimumSize(minimumSize);

        add(BorderLayout.CENTER, splitPane2);
        add(BorderLayout.SOUTH, inputWrapper);
        setVisible(true);
    }

    public void exit()
    {
        parser = null;
        this.dispose();
    }

    @Override
    public void onProcessDataUpdate()
    {

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
        String line = input.getText();
        String ret = parser.parseAndExecute(line);
        input.setText("");
        area.append("$" + line + "\n" + ret + "\n\n");
    }

    @Override
    public void onNewLogEvent(LogEvent event)
    {
        logArea.onNewLogRecord(event);
    }
}

package gui.config;

import com.github.g3force.configurable.ConfigRegistration;
import configuration.EConfigurable;
import org.apache.commons.configuration.HierarchicalConfiguration;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.net.URISyntaxException;
import java.util.logging.Logger;

/**
 * A tab for one config in the ConfigEditor. It provides a tree-view of the
 * xml-config with editing support and the possibilities to save, load and
 * switch config.
 *
 * @author Gero
 */
public class EditorView extends JPanel
{
    // --------------------------------------------------------------------------
    // --- constants and variables ----------------------------------------------
    // --------------------------------------------------------------------------
    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private static final long serialVersionUID = -7098099480668190062L;

    private static final boolean DISABLE_APPLY = false;
    private transient final JTreeTable treetable;
    private transient final Action applyAction;
    private transient ConfigXMLTreeTableModel model;

    // --------------------------------------------------------------------------
    // --- constructor ----------------------------------------------------------
    // --------------------------------------------------------------------------

    /**
     * @param title     t
     * @param configKey c
     * @param config    c
     * @param editable  e
     * @throws URISyntaxException exception
     */
    public EditorView(final String title, final String configKey, final HierarchicalConfiguration config,
                      final boolean editable) throws URISyntaxException
    {
        super();

        // Setup panel
        setLayout(new BorderLayout());

        // Setup upper part: Controls
        JPanel controls = new JPanel();
        controls.setLayout(new BoxLayout(controls, BoxLayout.LINE_AXIS));
        controls.setBorder(BorderFactory.createTitledBorder(title));
        controls.add(Box.createHorizontalGlue());
        add(controls, BorderLayout.NORTH);

        // Setup DropDown List
        JComboBox<EConfigurable> configList = new JComboBox<>();
        for (EConfigurable configType : EConfigurable.values())
        {
            configList.addItem(configType);
        }
        configList.setSelectedIndex(0);
        configList.setToolTipText("Load Configuration from Types");
        controls.add(Box.createHorizontalGlue());
        controls.add(configList);
        controls.add(Box.createHorizontalGlue());

        // Controls Load action
        Action reloadAction = new AbstractAction("Load")
        {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(final ActionEvent e)
            {
                HierarchicalConfiguration cfg = ConfigRegistration.
                        getConfig(configList.getSelectedItem().toString());
                updateModel(cfg);
            }
        };
        reloadAction.setEnabled(true);
        JButton reloadBtn = new JButton(reloadAction);
        reloadBtn.setToolTipText("Load config by reading from file");
        controls.add(reloadBtn);
        controls.add(Box.createHorizontalGlue());
        controls.add(Box.createHorizontalGlue());

        // Controls: Apply, Save, Switch, Reload
        applyAction = new AbstractAction("Apply and Save")
        {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(final ActionEvent e)
            {

                String config = configList.getSelectedItem().toString();
                ConfigRegistration.applyConfig(config);
                ConfigRegistration.save(config);
                applyAction.setEnabled(DISABLE_APPLY);
            }
        };
        applyAction.setEnabled(DISABLE_APPLY);
        JButton applyBtn = new JButton(applyAction);
        applyBtn.setToolTipText("Write current config to fields");
        controls.add(applyBtn);
        controls.add(Box.createHorizontalGlue());
        controls.add(Box.createHorizontalGlue());

        // Setup lower part: The actual editor
        JScrollPane scrollpane = new JScrollPane();
        scrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        add(scrollpane, BorderLayout.CENTER);

        // Finally: Add guimodel
        model = new ConfigXMLTreeTableModel(config);
        model.setEditable(editable);
        treetable = new JTreeTable(model);
        treetable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        treetable.getModel().addTableModelListener(event ->
        {
            if ((event.getType() == TableModelEvent.UPDATE) && (event.getFirstRow() == event.getLastRow()))
            {
                markDirty();
            }
        });
        scrollpane.add(treetable);
        scrollpane.setViewportView(treetable);
    }

    // --------------------------------------------------------------------------
    // --- methods --------------------------------------------------------------
    // --------------------------------------------------------------------------

    /**
     * @param config c
     */
    private void updateModel(final HierarchicalConfiguration config)
    {
        model = new ConfigXMLTreeTableModel(config);
        model.setEditable(true);
        treetable.setTreeTableModel(model);
        treetable.getModel().addTableModelListener(event ->
        {
            if ((event.getType() == TableModelEvent.UPDATE) && (event.getFirstRow() == event.getLastRow()))
            {
                markDirty();
            }
        });
    }

    void markDirty()
    {
        // Enable Buttons only
        applyAction.setEnabled(true);
    }
}

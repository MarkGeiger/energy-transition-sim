package gui.config;

import javax.swing.*;
import javax.swing.tree.TreeModel;


/**
 * The basic interface for any models for the {@link JTreeTable}. It features cell editing, customized rendering and
 *
 * @author Gero
 * @see JTreeTable
 */
public interface ITreeTableModel extends TreeModel
{
    /**
     * Returns the number ofs availible column.
     *
     * @return r
     */
    int getColumnCount();


    /**
     * Returns the name for column number <code>column</code>.
     *
     * @param column c
     * @return r
     */
    String getColumnName(int column);


    /**
     * Returns the type for column number <code>column</code>.
     *
     * @param column c
     * @return r
     */
    Class<?> getColumnClass(int column);


    /**
     * Returns the value to be displayed for node <code>node</code>,
     * at column number <code>column</code>.
     *
     * @param node   n
     * @param column c
     * @return r
     */
    Object getValueAt(Object node, int column);


    /**
     * Indicates whether the the value for node <code>node</code>,
     * at column number <code>column</code> is editable.
     *
     * @param node   mn
     * @param column c
     * @return r
     */
    boolean isCellEditable(Object node, int column);

    /**
     * @return guimodel is editable (default: <code>true</code>)
     */
    boolean isEditable();

    /**
     * @param editable e
     */
    void setEditable(boolean editable);

    /**
     * Sets the value for node <code>node</code>,
     * at column number <code>column</code>.
     *
     * @param aValue a
     * @param node   a
     * @param column a
     */
    void setValueAt(Object aValue, Object node, int column);


    /**
     * This lets the guimodel decide whether it wants to add/change something of the things the
     * {@link javax.swing.tree.DefaultTreeCellRenderer} does
     *
     * @param label a
     * @param node  a
     */
    void renderTreeCellComponent(JLabel label, Object node);

}

package gui.config;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.tree.TreePath;


/**
 * This is the {@link AbstractTableModel} implementation for the {@link javax.swing.JTable}-part of the
 * {@link JTreeTable}. It
 * therefore delegates the calls to the underlying {@link javax.swing.tree.TreeModel} (in {@link ITreeTableModel}).
 *
 * @author Gero
 * @see JTreeTable
 */
public class TreeTableModelAdapter extends AbstractTableModel
{
    /**
     *
     */
    private static final long serialVersionUID = -6298333095243382630L;

    final JTree tree;
    private transient final ITreeTableModel treeTableModel;


    /**
     * @param treeTableModel t
     * @param tree           t
     */
    public TreeTableModelAdapter(final ITreeTableModel treeTableModel, final JTree tree)
    {
        this.tree = tree;
        this.treeTableModel = treeTableModel;

        tree.addTreeExpansionListener(new TreeExpansionListener()
        {
            // Don't use fireTableRowsInserted() here; the selection guimodel
            // would get updated twice.
            @Override
            public void treeExpanded(final TreeExpansionEvent event)
            {
                fireTableDataChanged();
            }


            @Override
            public void treeCollapsed(final TreeExpansionEvent event)
            {
                fireTableDataChanged();
            }
        });

        // Install a TreeModelListener that can update the table when
        // tree changes. We use delayedFireTableDataChanged as we can
        // not be guaranteed the tree will have finished processing
        // the event before us.
        treeTableModel.addTreeModelListener(new TreeModelListener()
        {
            @Override
            public void treeNodesChanged(final TreeModelEvent e)
            {
                // Only one element changed
                if (e.getChildIndices().length == 0)
                {
                    final int row = TreeTableModelAdapter.this.tree.getRowForPath(e.getTreePath());
                    delayedFireTableRowsUpdated(row, row);
                } else
                {
                    delayedFireTableDataChanged();
                }
            }


            @Override
            public void treeNodesInserted(final TreeModelEvent e)
            {
                delayedFireTableDataChanged();
            }


            @Override
            public void treeNodesRemoved(final TreeModelEvent e)
            {
                delayedFireTableDataChanged();
            }


            @Override
            public void treeStructureChanged(final TreeModelEvent e)
            {
                delayedFireTableDataChanged();
            }
        });
    }


    // Wrappers, implementing TableModel interface.

    /**
     * (non-Javadoc)
     *
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount()
    {
        return treeTableModel.getColumnCount();
    }


    /**
     * (non-Javadoc)
     *
     * @see javax.swing.table.AbstractTableModel#getColumnName(int)
     */
    @Override
    public String getColumnName(final int column)
    {
        return treeTableModel.getColumnName(column);
    }


    /**
     * (non-Javadoc)
     *
     * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
     */
    @Override
    public Class<?> getColumnClass(final int column)
    {
        return treeTableModel.getColumnClass(column);
    }


    /**
     * (non-Javadoc)
     *
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount()
    {
        return tree.getRowCount();
    }


    protected Object nodeForRow(final int row)
    {
        final TreePath treePath = tree.getPathForRow(row);
        return treePath.getLastPathComponent();
    }


    /**
     * (non-Javadoc)
     *
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(final int row, final int column)
    {
        return treeTableModel.getValueAt(nodeForRow(row), column);
    }


    /**
     * (non-Javadoc)
     *
     * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
     */
    @Override
    public boolean isCellEditable(final int row, final int column)
    {
        return treeTableModel.isCellEditable(nodeForRow(row), column);
    }


    /**
     * (non-Javadoc)
     *
     * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
     */
    @Override
    public void setValueAt(final Object value, final int row, final int column)
    {
        treeTableModel.setValueAt(value, nodeForRow(row), column);
    }


    /**
     * Invokes fireTableDataChanged after all the pending events have been
     * processed. SwingUtilities.invokeLater is used to handle this.
     */
    protected void delayedFireTableDataChanged()
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                fireTableDataChanged();
            }
        });
    }


    /**
     * @param firstRow f
     * @param lastRow  l
     */
    protected void delayedFireTableRowsUpdated(final int firstRow, final int lastRow)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                fireTableRowsUpdated(firstRow, lastRow);
            }
        });
    }
}

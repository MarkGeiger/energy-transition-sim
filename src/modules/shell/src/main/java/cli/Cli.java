package cli;

import org.apache.commons.cli.*;

public class Cli
{
    private String[] args;
    private Options options = new Options();

    private String configPath = "./default_config.xml";

    public Cli(String[] args)
    {

        this.args = args;
        options.addOption("h", "help", false, "show help.");
        options.addOption("c", "config", true, "Path to config file.");
    }

    public void parse()
    {
        CommandLineParser parser = new BasicParser();
        CommandLine cmd;
        try
        {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("h"))
            {
                help();
            }

            if (cmd.hasOption("c"))
            {
                configPath = cmd.getOptionValue("c");
            }
        } catch (ParseException e)
        {
            help();
        }
    }

    private void help()
    {
        // This prints out some help
        HelpFormatter formater = new HelpFormatter();

        formater.printHelp("Main", options);
        System.exit(0);
    }

    public String getConfigPath()
    {
        return configPath;
    }
}

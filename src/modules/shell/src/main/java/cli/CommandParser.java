package cli;

import interfaces.ICommandObserver;

public class CommandParser
{
    private final ICommandObserver commandObserver;

    public CommandParser(ICommandObserver commandObserver)
    {
        this.commandObserver = commandObserver;
    }

    public String parseAndExecute(String line)
    {
        if ("help".equals(line))
        {
            return
                    "\navailable commands:\n" +
                            "    help:    show this text\n" +
                            "    run:     starts simulation\n" +
                            "    pause:   pauses simulation\n";
        } else if ("run".equals(line))
        {
            return commandObserver.onResume();
        } else if ("pause".equals(line))
        {
            return commandObserver.onPause();
        } else
        {
            return "<invalid command>";
        }
    }
}

package cli;

import interfaces.ICommandObserver;
import interfaces.IProcessDataObserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Shell implements Runnable, IProcessDataObserver
{
    private boolean running = true;

    private CommandParser parser;

    public Shell(ICommandObserver commandObserver)
    {
        parser = new CommandParser(commandObserver);
        System.out.println("Welcome to the Energy Transition Sim Shell!");
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run()
    {
        while (running)
        {
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(System.in));
            String line;
            try
            {
                line = reader.readLine();
                String ret = parser.parseAndExecute(line);
                System.out.print("$" + line + "\n" + ret + "\n\n");
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void setRunning(boolean running)
    {
        this.running = running;
    }

    public void exit()
    {
        parser = null;
        System.out.println("closing shell");
    }

    @Override
    public void onProcessDataUpdate()
    {

    }
}

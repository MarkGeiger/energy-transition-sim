# Energy-transition-sim
The **Energy Transition Simulator** is a tool to simulate possible 
paths to transit a nation into a fully renewable energy power supply.

1)

![Transition](/images/all.png)

2)

![Transition2](/images/ex.png)

# Prerequisites
- maven >= 3.0
- JDK >= 1.8

# Build and run 
- mvn install exec:java

# Data sources
Data for electricity production and consumption has been acquired using the data 
parsers provided by the OpenElectricityMap project: https://github.com/tmrowco/electricitymap-contrib
Input data can be gathered from the European Network of Transmission System Operators for Electricity (ENTSOE).

# Solar Position Algorithm
Literature:
Blanc, Ph, and Lucien Wald. "The SG2 algorithm for a fast and accurate computation of the position of the Sun for multi-decadal time period." Solar Energy 86.10 (2012): 3072-3083.

Rizvi, Arslan A., et al. "Sun position algorithm for sun tracking applications." IECON 2014-40th Annual Conference of the IEEE Industrial Electronics Society. IEEE, 2014.

Implementation is based on an older fortran implementation: http://libthesky.sourceforge.net/

# License
The complete project is licensed under the Apache2 license and thus free to use for anybody. 
For more information please refer to the LICENSE file.
